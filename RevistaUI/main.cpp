#include "UI/MainWindow.h"
#include <QCoreApplication>
#include <QApplication>

#include <Core/ApplicationContext.h>

int main(int argc, char *argv[])
{
  QCoreApplication::setOrganizationName("mva");
  QCoreApplication::setApplicationName("RevistaGit");

  QApplication a(argc, argv);

  RevistaGit::ApplicationContext appContext;

  RevistaGit::MainWindow w;
  w.show();

  return a.exec();
}
