#ifndef REVISTAGIT_TABGITLOG_H
#define REVISTAGIT_TABGITLOG_H

#include <QWidget>
#include "../Components/CustomWidgetItemDelegate.h"
#include <RevistaCore.h>

namespace RevistaGit {

namespace Ui {
class TabGitLog;
}

class TabGitLog : public QWidget
{
  Q_OBJECT

public:
  explicit TabGitLog(QWidget *parent = 0);
  ~TabGitLog();

  void setGitProject(GitProject *project);

private slots:

  void on_comboBox_activated(int index);

private:
  Ui::TabGitLog *ui;
  CustomWidgetItemDelegate *mDelegate;
  GitProject* mProject;
  GitLogModelUPtr mLogModel;
  GitRepositoryBranchesModelUPtr mBranchesModel;
};


} // namespace RevistaGit
#endif // REVISTAGIT_TABGITLOG_H
