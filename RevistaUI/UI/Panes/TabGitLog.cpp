#include "TabGitLog.h"
#include "ui_TabGitLog.h"

#include <Core/GitProject.h>
#include <Models/GitLogModel.h>
#include <Models/GitRepositoryBranchesModel.h>

#include <Reference.h>
#include <Oid.h>

#include "../Components/GitLogDelegate.h"

namespace RevistaGit {

TabGitLog::TabGitLog(QWidget *parent) :
  QWidget(parent),
  ui(new Ui::TabGitLog),
  mDelegate(new CustomWidgetItemDelegate(new GitLogDelegate()))
{
  ui->setupUi(this);
  ui->listView->setItemDelegate(mDelegate);
}

TabGitLog::~TabGitLog()
{
  delete ui;
  delete mDelegate;

}

void TabGitLog::setGitProject(GitProject *project)
{
  mProject = project;
  mLogModel.reset(new GitLogModel(mProject->repository()));
  ui->listView->setModel(mLogModel.get());

  mBranchesModel.reset(new GitRepositoryBranchesModel());
  mBranchesModel->setRepository(mProject->repository());
  ui->comboBox->setModel(mBranchesModel.get());
}

void TabGitLog::on_comboBox_activated(int index)
{
  const Git2Cpp::Reference * ref = mBranchesModel->getReference(index);
  if (ref)
    mLogModel->setStart(ref->target());
}

} // namespace RevistaGit

