#ifndef REVISTAGIT_GITPROJECTLISTVIEWDELEGATE_H
#define REVISTAGIT_GITPROJECTLISTVIEWDELEGATE_H

#include "ICustomWidgetItem.h"

namespace RevistaGit {

namespace Ui {
class GitProjectListViewDelegate;
}

class GitProjectListViewDelegate : public ICustomWidgetItem
{
  Q_OBJECT

public:
  explicit GitProjectListViewDelegate(QWidget *parent = 0);
  ~GitProjectListViewDelegate();


  // ICustomWidgetItem interface
  void setData(const QModelIndex &index) override;

private:
  Ui::GitProjectListViewDelegate *ui;

};


} // namespace RevistaGit
#endif // REVISTAGIT_GITPROJECTLISTVIEWDELEGATE_H
