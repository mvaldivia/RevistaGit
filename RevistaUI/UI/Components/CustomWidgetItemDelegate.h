#ifndef CUSTOMWIDGETITEMDELEGATE_H
#define CUSTOMWIDGETITEMDELEGATE_H

#include <QStyledItemDelegate>
#include "ICustomWidgetItem.h"

namespace RevistaGit {

class CustomWidgetItemDelegate : public QStyledItemDelegate
{
  Q_OBJECT
public:
  explicit CustomWidgetItemDelegate(ICustomWidgetItem *item = 0);
  ~CustomWidgetItemDelegate();

  // QAbstractItemDelegate interface
  void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
  QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const override;

private:
  ICustomWidgetItem *mItem;
};

} // namespace RevistaGit

#endif // CUSTOMWIDGETITEMDELEGATE_H
