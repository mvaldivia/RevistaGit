#include "GitProjectListViewDelegate.h"
#include "ui_GitProjectListViewDelegate.h"

#include <Models/GitProjectsModel.h>

namespace RevistaGit {

GitProjectListViewDelegate::GitProjectListViewDelegate(QWidget *parent) :
  ICustomWidgetItem(parent),
  ui(new Ui::GitProjectListViewDelegate)
{
  ui->setupUi(this);
}

GitProjectListViewDelegate::~GitProjectListViewDelegate()
{
  delete ui;
}

void GitProjectListViewDelegate::setData(const QModelIndex &index)
{
  ui->projectName->setText(index.data(GitProjectsModel::NameRole).toString());
  ui->projectPath->setText(index.data(GitProjectsModel::PathRole).toString());
}

} // namespace RevistaGit
