#ifndef REVISTAGIT_GITLOGDELEGATE_H
#define REVISTAGIT_GITLOGDELEGATE_H

#include "ICustomWidgetItem.h"

namespace RevistaGit {

namespace Ui {
class GitLogDelegate;
}

class GitLogDelegate : public ICustomWidgetItem
{
  Q_OBJECT

public:
  explicit GitLogDelegate(QWidget *parent = 0);
  ~GitLogDelegate();

  // ICustomWidgetItem interface
  void setData(const QModelIndex &index) override;

private:
  Ui::GitLogDelegate *ui;
};


} // namespace RevistaGit
#endif // REVISTAGIT_GITLOGDELEGATE_H
