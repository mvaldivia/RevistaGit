#include "GitLogDelegate.h"
#include "ui_GitLogDelegate.h"

#include <Models/GitLogModel.h>

namespace RevistaGit {

GitLogDelegate::GitLogDelegate(QWidget *parent) :
  ICustomWidgetItem(parent),
  ui(new Ui::GitLogDelegate)
{
  ui->setupUi(this);
}

GitLogDelegate::~GitLogDelegate()
{
  delete ui;
}

void GitLogDelegate::setData(const QModelIndex &index)
{
  ui->commitTitle->setText(index.data(GitLogModel::TitleRole).toString());
  ui->commitAuthor->setText(index.data(GitLogModel::AuthorRole).toString());
  ui->commitDate->setText(index.data(GitLogModel::DateRole).toString());
}

} // namespace RevistaGit


