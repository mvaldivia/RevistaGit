#include "CustomWidgetItemDelegate.h"

#include <QPainter>

namespace RevistaGit {

CustomWidgetItemDelegate::CustomWidgetItemDelegate(ICustomWidgetItem *item)
  : mItem(item)
{

}

CustomWidgetItemDelegate::~CustomWidgetItemDelegate()
{
  if (mItem)
    delete mItem;
}

void CustomWidgetItemDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
  QStyledItemDelegate::paint(painter, option, index);
  // TODO remove base implementation + highlight paint
  if (mItem)
  {
    painter->save();
    mItem->setData(index);
    mItem->resize(option.rect.size());
    painter->translate(option.rect.topLeft());
    mItem->render(painter, QPoint(), QRegion(), QWidget::DrawChildren);
    painter->restore();
  }
}

QSize CustomWidgetItemDelegate::sizeHint(const QStyleOptionViewItem & /*option*/, const QModelIndex &index) const
{
  if (mItem)
  {
    mItem->setData(index);
    return mItem->sizeHint();
  }
  else
    return QSize();
}

} // namespace RevistaGit
