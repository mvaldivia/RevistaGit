#ifndef ICUSTOMWIDGETITEM_H
#define ICUSTOMWIDGETITEM_H

#include <QWidget>

namespace RevistaGit {

class ICustomWidgetItem : public QWidget
{
  Q_OBJECT

public:
  explicit ICustomWidgetItem(QWidget *parent) : QWidget(parent)
  {

  }

  virtual ~ICustomWidgetItem() {

  }

  virtual void setData(const QModelIndex & index) = 0;
};

}

#endif // ICUSTOMWIDGETITEM_H
