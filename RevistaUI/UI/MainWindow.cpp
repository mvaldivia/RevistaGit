#include "MainWindow.h"
#include "ui_MainWindow.h"
#include "GitProjectPropertiesDialog.h"
#include "Models/GitProjectsModel.h"
#include "Models/GitCommitTreeModel.h"
#include "Models/GitLogModel.h"
#include "Core/GitProject.h"
#include "Core/Settings.h"
#include "Components/GitProjectListViewDelegate.h"

#include <Repository.h>
#include <Reference.h>
#include <Commit.h>
#include <Tree.h>
#include <Object.h>

#include <QSettings>
#include <QMessageBox>
#include <QDir>
#include <QUrl>
#include <QDesktopServices>

#include <iostream>

namespace RevistaGit {

MainWindow::MainWindow(QWidget *parent)
  : QMainWindow(parent)
  , ui(new Ui::MainWindow)
  , mGitProjectsModel(new GitProjectsModel(this))
  , mGitCommitTreeModel(nullptr)
  , mCurrentProject(nullptr)
  , mProjectListViewDelegate(new CustomWidgetItemDelegate(new GitProjectListViewDelegate()))
{
  ui->setupUi(this);
  setupUI();
}

MainWindow::~MainWindow()
{
  saveUIState();
  delete ui;
  delete mGitCommitTreeModel;
  delete mGitProjectsModel;
  delete mProjectListViewDelegate;
}

void MainWindow::setupUI()
{
  Settings settings;
  mGitProjectsModel->setProjectsList(settings.loadGitProjects());
  ui->gitProjectsListView->setItemDelegate(mProjectListViewDelegate);

  ui->gitProjectsListView->setModel(mGitProjectsModel);

  // Context Menu
  QList<QAction *> contextMenuItems;
  contextMenuItems << ui->actionRepositoryAddExisting;
  contextMenuItems << ui->actionRepositoryClone;
  contextMenuItems << ui->actionRepositoryCreate;
  contextMenuItems << ui->actionRepositoryRemove;
  QAction *sep = new QAction(this);
  sep->setSeparator(true);
  contextMenuItems << sep;
  contextMenuItems << ui->actionOpenInBrowser;
  ui->gitProjectsListView->addActions(contextMenuItems);

  // UI State
  loadUIState();
}

void MainWindow::loadUIState()
{
    QSettings settings;

    settings.beginGroup("mainwindow");
    restoreGeometry(settings.value("geometry").toByteArray());
    ui->splitter->restoreState(settings.value("mainsplitter").toByteArray());
    settings.endGroup();
}

void MainWindow::saveUIState()
{
    QSettings settings;

    settings.beginGroup("mainwindow");
    settings.setValue("geometry", saveGeometry());
    settings.setValue("mainsplitter", ui->splitter->saveState());
    settings.endGroup();
}

void MainWindow::openNewGitProjectDialog(int mode)
{
  GitProjectUPtr proj(new GitProject());

  GitProjectPropertiesDialog::DialogModeT dMode = static_cast<GitProjectPropertiesDialog::DialogModeT>(mode);
  GitProjectPropertiesDialog dlg(this, dMode, proj.get());
  if (dlg.exec() == QDialog::Accepted)
  {
    mGitProjectsModel->addProject(proj.release());
    Settings settings;
    settings.saveGitProjects(mGitProjectsModel->getProjectsList());
  }
}

void MainWindow::on_actionRepositoryAddExisting_triggered()
{
  openNewGitProjectDialog(GitProjectPropertiesDialog::ExistingRepository);
}

void MainWindow::on_actionRepositoryCreate_triggered()
{
  openNewGitProjectDialog(GitProjectPropertiesDialog::Creation);
}

void MainWindow::on_actionRepositoryClone_triggered()
{
  openNewGitProjectDialog(GitProjectPropertiesDialog::Clone);
}

void MainWindow::on_actionRepositoryRemove_triggered()
{
  if (!ui->gitProjectsListView->selectionModel()->hasSelection())
    return;

  QModelIndex index = ui->gitProjectsListView->selectionModel()->currentIndex();
  QString repoName = index.data().toString();
  // TODO add param to erase from disk
  if (QMessageBox::Yes == QMessageBox::question(this, tr("Delete repository"),
                                                tr("Do you really want to remove the project \"%1\" ?").arg(repoName),
                                                QMessageBox::Yes | QMessageBox::No,
                                                QMessageBox::No))
  {
    mGitProjectsModel->removeProject(index);
  }
}

void MainWindow::on_actionOpenInBrowser_triggered()
{
  if (!ui->gitProjectsListView->selectionModel()->hasSelection())
    return;

  QModelIndex index = ui->gitProjectsListView->selectionModel()->currentIndex();
  QString repoPath = index.data(GitProjectsModel::PathRole).toString();
  QDir path(repoPath);
  if (!path.exists())
  {
    if (QMessageBox::Yes == QMessageBox::critical(this, tr("Error opening repository folder"),
                                                  tr("The repository folder  does not exist !\nDo you want to remove the project \"%1\" ?").arg(index.data().toString()),
                                                  QMessageBox::Yes | QMessageBox::No,
                                                  QMessageBox::No))
    {
      mGitProjectsModel->removeProject(index);
    }
  }
  else
    QDesktopServices::openUrl(QUrl::fromLocalFile(repoPath));
}

void MainWindow::displayGitProject(GitProject * project)
{
  mCurrentProject = project;
  const Git2Cpp::Repository * repo = mCurrentProject->repository();
  Git2Cpp::Reference head = repo->getHead();
  Git2Cpp::Object obj = repo->lookup<Git2Cpp::Object>(head.target());
  Git2Cpp::Commit commit(std::move(obj));

  std::cout << "Info on commit " << commit.getId() << std::endl;
  std::cout << "Author : " << commit.getAuthor() << std::endl;
  std::cout << "Committer : " << commit.getCommitter() << std::endl;
  std::cout << "Summary : " << commit.getSummary() << std::endl;
  std::cout << "Body : " << commit.getBody() << std::endl;
  std::cout << "Message : " << commit.getMessage() << std::endl;

  if (mGitCommitTreeModel != nullptr)
    delete mGitCommitTreeModel;
  mGitCommitTreeModel = new GitCommitTreeModel(this, &commit);
  ui->gitCommitTreeView->setModel(mGitCommitTreeModel);

  ui->tabLog->setGitProject(mCurrentProject);
}

void MainWindow::on_gitProjectsListView_doubleClicked(const QModelIndex &index)
{
  GitProject * project = mGitProjectsModel->getProject(index);
  QString res = project->open();
  if (res.isEmpty())
    displayGitProject(project);
  else
  {
    // TODO add param to erase from disk
    if (QMessageBox::Yes == QMessageBox::question(this, tr("Error opening repository"),
                                                  tr("Do you want to remove the project \"%1\" ?").arg(project->getName()),
                                                  QMessageBox::Yes | QMessageBox::No,
                                                  QMessageBox::No))
    {
      mGitProjectsModel->removeProject(index);
    }
  }
}

void MainWindow::on_gitCommitTreeView_doubleClicked(const QModelIndex &index)
{
    QVariant tmp = index.data(GitCommitTreeModel::OidRole);
    std::string oidStr = tmp.toString().toStdString();
    Git2Cpp::Oid oid = oidStr;
    try {
      //Git2Cpp::Commit commit = mCurrentProject->getRepository()->lookup<Git2Cpp::Commit>(oid);
      //std::cout << "Found commit : " << commit.getId() << std::endl;
      Git2Cpp::Object obj = mCurrentProject->repository()->lookup<Git2Cpp::Object>(oid);
      std::cout << "Found object : " << obj.getId() << ":"<< obj.typeStr() << std::endl;
    } catch (std::exception &ex) {
      std::cout << "Lookup Exception : " << ex.what() << std::endl;
    }

}

} // namespace RevistaGit
