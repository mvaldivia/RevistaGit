#ifndef REVISTAGIT_GITPROJECTPROPERTIESDIALOG_H
#define REVISTAGIT_GITPROJECTPROPERTIESDIALOG_H

#include <QDialog>


namespace RevistaGit {

namespace Ui {
class GitProjectPropertiesDialog;
}

class GitProject;

class GitProjectPropertiesDialog : public QDialog
{
  Q_OBJECT

public:

  typedef enum {
    Creation,
    ExistingRepository,
    Clone,
    Edit
  } DialogModeT;

  explicit GitProjectPropertiesDialog(QWidget *parent = 0, DialogModeT mode = Creation, GitProject *project = 0);
  ~GitProjectPropertiesDialog();

private slots:
  void on_btBrowse_clicked();
  void done(int) override;

private:
  Ui::GitProjectPropertiesDialog *ui;
  GitProject *mProject;

  void setupUI(DialogModeT mode);
  void showError(const QString & errMsg);
  QString getProjectName() const;
};

} // namespace RevistaGit

#endif // REVISTAGIT_GITPROJECTPROPERTIESDIALOG_H
