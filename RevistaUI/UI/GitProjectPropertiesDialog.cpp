#include "GitProjectPropertiesDialog.h"
#include "ui_GitProjectPropertiesDialog.h"

#include <QMessageBox>
#include <QFileDialog>
#include <QDir>
#include <QString>

#include "Core/GitProject.h"

namespace RevistaGit {

GitProjectPropertiesDialog::GitProjectPropertiesDialog(QWidget *parent, DialogModeT mode, GitProject *project)
  : QDialog(parent)
  , ui(new Ui::GitProjectPropertiesDialog)
  , mProject(project)
{
  ui->setupUi(this);
  setupUI(mode);
}

GitProjectPropertiesDialog::~GitProjectPropertiesDialog()
{
  delete ui;
}
// #############################################################
// Private
void GitProjectPropertiesDialog::setupUI(DialogModeT mode)
{
  QString title;
  bool showURI = false;

  switch (mode) {
  case Creation:
    title = tr("Create repository");
    break;
  case Clone:
    title = tr("Clone repository");
    showURI = true;
    break;
  case ExistingRepository:
    title = tr("Add existing repository");
    break;
  default:
    title = tr("Edit repository");
    break;
  }

  setWindowTitle(title);
  ui->lbURI->setVisible(showURI);
  ui->leURI->setVisible(showURI);
}

QString GitProjectPropertiesDialog::getProjectName() const
{
  // open existing repo
  QDir path(ui->lePath->text());
  return path.dirName();
}

void GitProjectPropertiesDialog::showError(const QString &errMsg)
{
  QMessageBox msgbox(this);
  msgbox.setText(errMsg);
  msgbox.setIcon(QMessageBox::Critical);
  msgbox.exec();
}

// #############################################################
// Private slots
void GitProjectPropertiesDialog::on_btBrowse_clicked()
{
  QString path = QFileDialog::getExistingDirectory(this, tr("Choose folder"));
  if (path.isEmpty()) return;

  ui->lePath->setText(QDir::toNativeSeparators(path));
  if (ui->leName->text().isEmpty())
    ui->leName->setText(getProjectName());
}

void GitProjectPropertiesDialog::done(int r)
{
  if (r == QDialog::Accepted)
  {
    mProject->setName(ui->leName->text());
    mProject->setPath(ui->lePath->text());

    QString err = mProject->open();

    if (!err.isEmpty())
    {
      showError(err);
      return;
    }

  }
  QDialog::done(r);
}

} // namespace RevistaGit
