#ifndef REVISTAGIT_MAINWINDOW_H
#define REVISTAGIT_MAINWINDOW_H

#include <QMainWindow>
#include "Components/CustomWidgetItemDelegate.h"

namespace RevistaGit {

namespace Ui {
class MainWindow;
}

class GitProject;
class GitProjectsModel;
class GitCommitTreeModel;


class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = 0);
  ~MainWindow();

private slots:
  void on_actionRepositoryAddExisting_triggered();
  void on_actionRepositoryCreate_triggered();
  void on_actionRepositoryClone_triggered();
  void on_actionRepositoryRemove_triggered();

  void on_actionOpenInBrowser_triggered();

  void on_gitProjectsListView_doubleClicked(const QModelIndex &index);

  void on_gitCommitTreeView_doubleClicked(const QModelIndex &index);


private:
  Ui::MainWindow *ui;
  GitProjectsModel *mGitProjectsModel;
  GitCommitTreeModel *mGitCommitTreeModel;
  GitProject * mCurrentProject;
  CustomWidgetItemDelegate *mProjectListViewDelegate;


  void setupUI();
  void loadUIState();
  void saveUIState();

  void openNewGitProjectDialog(int mode);
  void displayGitProject(GitProject *project);
};

} // namespace RevistaGit

#endif // REVISTAGIT_MAINWINDOW_H
