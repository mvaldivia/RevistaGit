#-------------------------------------------------
#
# Project created by QtCreator 2016-10-27T19:57:13
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = RevistaUI
TEMPLATE = app


SOURCES += main.cpp \
    UI/GitProjectPropertiesDialog.cpp \
    UI/MainWindow.cpp \
    UI/Components/CustomWidgetItemDelegate.cpp \
    UI/Panes/TabGitLog.cpp \
    UI/Components/GitLogDelegate.cpp \
    UI/Components/GitProjectListViewDelegate.cpp

HEADERS  += \
    UI/GitProjectPropertiesDialog.h \
    UI/MainWindow.h \
    UI/Components/CustomWidgetItemDelegate.h \
    UI/Components/ICustomWidgetItem.h \
    UI/Panes/TabGitLog.h \
    UI/Components/GitLogDelegate.h \
    UI/Components/GitProjectListViewDelegate.h

FORMS    += \
    UI/GitProjectPropertiesDialog.ui \
    UI/MainWindow.ui \
    UI/Panes/TabGitLog.ui \
    UI/Components/GitLogDelegate.ui \
    UI/Components/GitProjectListViewDelegate.ui

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../Git2Cpp/release/ -lGit2Cpp
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../Git2Cpp/debug/ -lGit2Cpp
else:unix: LIBS += -L$$OUT_PWD/../Git2Cpp/ -lGit2Cpp

INCLUDEPATH += $$PWD/../Git2Cpp
DEPENDPATH += $$PWD/../Git2Cpp

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../RevistaCore/release/ -lRevistaCore
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../RevistaCore/debug/ -lRevistaCore
else:unix: LIBS += -L$$OUT_PWD/../RevistaCore/ -lRevistaCore

INCLUDEPATH += $$PWD/../RevistaCore
DEPENDPATH += $$PWD/../RevistaCore

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../RevistaCore/release/libRevistaCore.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../RevistaCore/debug/libRevistaCore.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../RevistaCore/release/RevistaCore.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../RevistaCore/debug/RevistaCore.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../RevistaCore/libRevistaCore.a
