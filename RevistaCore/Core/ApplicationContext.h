#ifndef REVISTAGIT_APPLICATIONCONTEXT_H
#define REVISTAGIT_APPLICATIONCONTEXT_H

#include "RevistaGit_global.h"

#include <memory>
#include <Models/GitProjectsModel.h>

namespace RevistaGit {

class GitProjectsModel;

class ApplicationContext : public QObject
{
  Q_OBJECT
  Q_PROPERTY(GitProjectsModel * projectsModel READ projectsModel NOTIFY projectsModelChanged)

public:
  explicit ApplicationContext();
  ~ApplicationContext();

  void loadPrefs();
  void savePrefs();

  // Properties
  GitProjectsModel *projectsModel() const;

signals:
  void projectsModelChanged();

private:
  std::unique_ptr<GitProjectsModel> mGitProjectsModel;

};

} // namespace RevistaGit

#endif // REVISTAGIT_APPLICATIONCONTEXT_H
