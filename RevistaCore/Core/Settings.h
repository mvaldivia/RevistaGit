#ifndef REVISTAGIT_SETTINGS_H
#define REVISTAGIT_SETTINGS_H

#include "RevistaGit_global.h"

namespace RevistaGit {

class Settings
{
public:  
  Settings();
  ~Settings();

  GitProjectsList loadGitProjects();
  void saveGitProjects(const GitProjectsList &projectList);

private:
};

} // namespace RevistaGit

#endif // REVISTAGIT_SETTINGS_H
