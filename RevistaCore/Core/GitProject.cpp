#include "GitProject.h"
#include <Repository.h>

namespace RevistaGit {

GitProject::GitProject(QObject *parent)
  : QObject(parent)
  , mRepository(nullptr)
{

}

GitProject::~GitProject()
{

}

bool GitProject::isValid() const
{
  return (mRepository != nullptr);
}

const QString & GitProject::getName() const
{
  return mName;
}


void GitProject::setName(const QString & name)
{
  mName = name;
  emit nameChanged();
}

const QString & GitProject::getPath() const
{
  return mRepoPath;
}

void GitProject::setPath(const QString & path)
{
  mRepoPath = path;
  emit pathChanged();
}

const Git2Cpp::Repository *GitProject::repository() const
{
  return mRepository.get();
}

QString GitProject::open()
{
  mRepository.reset(new Git2Cpp::Repository());
  try
  {
    mRepository->open(mRepoPath.toStdString());
    return "";
  }
  catch (std::exception &ex)
  {
    mRepository.reset();
    return ex.what();
  }
}

} // namespace RevistaGit
