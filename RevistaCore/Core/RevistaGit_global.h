#ifndef REVISTAGIT_GLOBAL_H
#define REVISTAGIT_GLOBAL_H

#ifdef __GNUC__
#define NO_EXCEPT noexcept
#else
#define NO_EXCEPT
#endif

#include <QVector>

namespace RevistaGit {

  class GitProject;
  typedef QVector<GitProject*> GitProjectsList;

}

#endif // REVISTAGIT_GLOBAL_H
