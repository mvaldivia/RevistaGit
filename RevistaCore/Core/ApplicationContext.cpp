#include "ApplicationContext.h"
#include "Models/GitProjectsModel.h"
#include "Core/Settings.h"
#include "Core/GitProject.h"

namespace RevistaGit {

ApplicationContext::ApplicationContext()
  : mGitProjectsModel(new GitProjectsModel())
{
  loadPrefs();
}

ApplicationContext::~ApplicationContext()
{
}

void ApplicationContext::loadPrefs()
{
  Settings settings;
  mGitProjectsModel->setProjectsList(settings.loadGitProjects());
}

void ApplicationContext::savePrefs()
{

}

// Properties
GitProjectsModel *ApplicationContext::projectsModel() const
{
  return mGitProjectsModel.get();
}



} // namespace RevistaGit
