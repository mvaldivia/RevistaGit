#include "Settings.h"

#include <QSettings>

#include "GitProject.h"

#define SETTING_PROJECTS "projects"
#define SETTING_REPO_NAME "name"
#define SETTING_REPO_PATH "path"

namespace RevistaGit {

Settings::Settings()
{
}

Settings::~Settings()
{
}

GitProjectsList Settings::loadGitProjects()
{
  QSettings settings;
  GitProjectsList list;

  int size = settings.beginReadArray(SETTING_PROJECTS);
  for (int i = 0; i < size; i++)
  {
    settings.setArrayIndex(i);

    GitProject * proj = new GitProject();
    proj->setName(settings.value(SETTING_REPO_NAME).toString());
    proj->setPath(settings.value(SETTING_REPO_PATH).toString());

    list.append(proj);
  }
  settings.endArray();

  return list;
}

void Settings::saveGitProjects(const GitProjectsList &projectList)
{
  QSettings settings;

  settings.beginWriteArray(SETTING_PROJECTS);
  for (int i = 0; i < projectList.size(); i++)
  {
    settings.setArrayIndex(i);
    settings.setValue(SETTING_REPO_NAME, projectList.at(i)->getName());
    settings.setValue(SETTING_REPO_PATH, projectList.at(i)->getPath());

  }
  settings.endArray();
}

} // namespace RevistaGit
