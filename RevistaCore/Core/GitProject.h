#ifndef REVISTAGIT_GITPROJECT_H
#define REVISTAGIT_GITPROJECT_H

#include <memory>
#include <QObject>
#include <QString>

#include "RevistaCore.h"

namespace RevistaGit {

class GitProject : public QObject
{
  Q_OBJECT
  Q_PROPERTY(QString name READ getName WRITE setName NOTIFY nameChanged)
  Q_PROPERTY(QString path READ getPath WRITE setPath NOTIFY pathChanged)
  Q_PROPERTY(const Git2Cpp::Repository * repository READ repository NOTIFY repositoryChanged)
public:

  explicit GitProject(QObject *parent = 0);
  ~GitProject();

  const QString & getName() const;
  void setName(const QString & name);

  const QString & getPath() const;
  void setPath(const QString & path);

  bool isValid() const;

  const Git2Cpp::Repository *repository() const;

signals:
  void nameChanged();
  void pathChanged();
  void repositoryChanged();

public slots:
  QString open();


private:
  QString mName;
  QString mRepoPath;
  std::unique_ptr<Git2Cpp::Repository> mRepository;
};

} // namespace RevistaGit


#endif // REVISTAGIT_GITPROJECT_H
