#-------------------------------------------------
#
# Project created by QtCreator 2016-11-06T23:15:58
#
#-------------------------------------------------

QT       -= gui

TARGET = RevistaCore
TEMPLATE = lib
CONFIG += staticlib

SOURCES += RevistaCore.cpp\
    Core/GitProject.cpp \
    Models/GitProjectsModel.cpp \
    Core/Settings.cpp \
    Core/ApplicationContext.cpp \
    Models/GitCommitTreeModel.cpp \
    Models/GitLogModel.cpp \
    Models/GitRepositoryBranchesModel.cpp \
    Models/GitCommitTreeModelLogWorker.cpp \
    Models/GitRepositoryStatusModel.cpp

HEADERS += RevistaCore.h\
    Core/GitProject.h \
    Models/GitProjectsModel.h \
    Core/Settings.h \
    Core/ApplicationContext.h \
    Models/GitCommitTreeModel.h \
    Models/GitLogModel.h \
    Core/RevistaGit_global.h \
    Models/GitRepositoryBranchesModel.h \
    Models/GitCommitTreeModelLogWorker.h \
    Models/GitRepositoryStatusModel.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../Git2Cpp/release/ -lGit2Cpp
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../Git2Cpp/debug/ -lGit2Cpp
else:unix: LIBS += -L$$OUT_PWD/../Git2Cpp/ -lGit2Cpp

INCLUDEPATH += $$PWD/../Git2Cpp
DEPENDPATH += $$PWD/../Git2Cpp
