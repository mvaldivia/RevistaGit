#ifndef REVISTACORE_H
#define REVISTACORE_H

#include <memory>
#include <QMetaType>

#define RG_REGISTER_PTR_TO_QML(T) \
  Q_DECLARE_METATYPE(T*) \
  Q_DECLARE_OPAQUE_POINTER(T*) \
  Q_DECLARE_METATYPE(const T*) \
  Q_DECLARE_OPAQUE_POINTER(const T*)

namespace RevistaGit {

#define RG_DECL_TYPE(T) class T; \
  typedef std::unique_ptr<T> T##UPtr

RG_DECL_TYPE(GitProject);
RG_DECL_TYPE(GitLogModel);
RG_DECL_TYPE(GitRepositoryBranchesModel);

} // namespace RevistaGit

namespace Git2Cpp {
  class Repository;
  class Reference;
  class Commit;
}

RG_REGISTER_PTR_TO_QML(Git2Cpp::Repository)
RG_REGISTER_PTR_TO_QML(Git2Cpp::Reference)
RG_REGISTER_PTR_TO_QML(Git2Cpp::Commit)

#endif // REVISTACORE_H
