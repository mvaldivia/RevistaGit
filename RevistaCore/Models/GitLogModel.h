#ifndef REVISTAGIT_GITLOGMODEL_H
#define REVISTAGIT_GITLOGMODEL_H

#include <memory>
#include <vector>
#include <QAbstractListModel>

#include "RevistaCore.h"

namespace Git2Cpp {
  class Commit;
  class RevWalk;
  class Oid;
}

namespace RevistaGit {

class GitLogModel : public QAbstractListModel
{
  Q_OBJECT
  Q_PROPERTY(const Git2Cpp::Repository * repository READ repository WRITE setRepository NOTIFY repositoryChanged)

public:
  explicit GitLogModel(const Git2Cpp::Repository * repository = nullptr, QObject *parent = 0);
  ~GitLogModel();

  enum LogRole {
    GuidRole = Qt::UserRole + 1,
    TitleRole,
    AuthorRole,
    DateRole
  };

  // Basic functionality:
  int rowCount(const QModelIndex &parent = QModelIndex()) const override;
  QHash<int, QByteArray> roleNames() const override;
  QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

  void setStart(const Git2Cpp::Oid & oid);
  Q_INVOKABLE void setStart(const Git2Cpp::Reference *ref);

  // Properties
  const Git2Cpp::Repository *repository() const;
  void setRepository(const Git2Cpp::Repository *repository);

signals:
  void repositoryChanged();

protected:
  bool canFetchMore(const QModelIndex &parent) const override;
  void fetchMore(const QModelIndex &parent) override;

private:
  std::vector<Git2Cpp::Commit> mCommitList;
  const Git2Cpp::Repository *mRepository;
  std::unique_ptr<Git2Cpp::RevWalk> mRevWalk;

  int mPageNbItems;
  bool mCanFetchMore;

};

} // namespace RevistaGit

#endif // GITLOGMODEL_H
