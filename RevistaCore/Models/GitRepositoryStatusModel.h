#ifndef REVISTAGIT_GITREPOSITORYSTATUSMODEL_H
#define REVISTAGIT_GITREPOSITORYSTATUSMODEL_H

#include <memory>
#include <QAbstractListModel>
#include <vector>
#include <utility>
#include "RevistaCore.h"

namespace Git2Cpp {
  class Status;
}

namespace RevistaGit {

class GitRepositoryStatusModel : public QAbstractListModel
{
  Q_OBJECT
  Q_PROPERTY(const Git2Cpp::Repository * repository READ repository WRITE setRepository NOTIFY repositoryChanged)
  Q_PROPERTY(Type type READ type WRITE setType NOTIFY typeChanged)

public:
  explicit GitRepositoryStatusModel(QObject *parent = 0);
  ~GitRepositoryStatusModel();

  enum class Type
  {
    INDEX,
    WORK_DIR
  };
  Q_ENUM(Type)

  enum EntryStatus {
    Current,
    New,
    Modified,
    Deleted,
    Renamed,
    Unreadable,
    TypeChange,
    Ignored,
    Conflicted
  };
  Q_ENUM(EntryStatus)
  typedef enum EntryStatus TEntryStatus;

  enum StatusRoles
  {
    FileName = Qt::UserRole + 1,
    FileStatus
  };

  void setRepository(const Git2Cpp::Repository *repository);
  Type type() const;
  void setType(Type type);

  // Basic functionality:
  int rowCount(const QModelIndex &parent = QModelIndex()) const override;

  QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

signals:
  void repositoryChanged();
  void typeChanged();

private:
  Type mType;
  std::vector<std::pair<QString, TEntryStatus>> mFilesStatus;

  const Git2Cpp::Repository *repository();


  // QAbstractItemModel interface
public:
  QHash<int, QByteArray> roleNames() const override;
};

} // namespace RevistaGit

Q_DECLARE_METATYPE(RevistaGit::GitRepositoryStatusModel::Type)
Q_DECLARE_METATYPE(RevistaGit::GitRepositoryStatusModel::EntryStatus)

#endif // REVISTAGIT_GITREPOSITORYSTATUSMODEL_H
