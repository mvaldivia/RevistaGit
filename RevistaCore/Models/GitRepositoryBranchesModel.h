#ifndef REVISTAGIT_GITREPOSITORYBRANCHESMODEL_H
#define REVISTAGIT_GITREPOSITORYBRANCHESMODEL_H

#include <QAbstractListModel>
#include <QVector>

#include "RevistaCore.h"

namespace RevistaGit {

class GitRepositoryBranchesModel : public QAbstractListModel
{
  Q_OBJECT
  Q_PROPERTY(const Git2Cpp::Repository * repository READ repository WRITE setRepository NOTIFY repositoryChanged)

public:
  explicit GitRepositoryBranchesModel(QObject *parent = 0);
  ~GitRepositoryBranchesModel();

  // Basic functionality:
  enum BranchRoles {
    GuidRole = Qt::UserRole + 1,
    ShortNameRole,
    NameRole
  };

  int rowCount(const QModelIndex &parent = QModelIndex()) const override;
  QHash<int, QByteArray> roleNames() const override;
  QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

  void setRepository(const Git2Cpp::Repository *repo);

  Q_INVOKABLE const Git2Cpp::Reference *getReference(int index);

signals:
  void repositoryChanged();

private:
  QVector<Git2Cpp::Reference*> mBranches;

  const Git2Cpp::Repository *repository() const;

};

} // namespace RevistaGit

#endif // REVISTAGIT_GITREPOSITORYBRANCHESMODEL_H
