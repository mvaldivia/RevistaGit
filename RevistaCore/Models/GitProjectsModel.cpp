#include "GitProjectsModel.h"
#include "Core/GitProject.h"
#include <stdexcept>

namespace RevistaGit {

GitProjectsModel::GitProjectsModel(QObject *parent)
  : QAbstractListModel(parent)
 // , mGitProjectsList(new GitProjectsList())
{

}

GitProjectsModel::~GitProjectsModel()
{
    Q_FOREACH(GitProject * proj, mGitProjectsList)
      delete proj;
}

int GitProjectsModel::rowCount(const QModelIndex &/*parent*/) const
{
  return mGitProjectsList.size();
}

QVariant GitProjectsModel::data(const QModelIndex &index, int role) const
{
  if (!index.isValid() || index.row() < 0 || index.row() > mGitProjectsList.size())
    return QVariant();

  const GitProject * project = mGitProjectsList.at(index.row());
  switch (role)
  {
  case NameRole:
    return project->getName();
  case PathRole:
    return project->getPath();
  default:
    return QVariant();
  }
}

QHash<int, QByteArray> GitProjectsModel::roleNames() const
{
  QHash<int, QByteArray> roles;
  roles[NameRole] = "name";
  roles[PathRole] = "path";
  return roles;
}

QModelIndex GitProjectsModel::index(int row, int column, const QModelIndex &parent) const
{
  if (!hasIndex(row, column, parent))
    return QModelIndex();

  if (parent.isValid())
    return QModelIndex();

  return createIndex(row, column, nullptr);
}

GitProject *GitProjectsModel::getProject(const QModelIndex &index)
{
  if (!index.isValid() || index.row() < 0 || index.row() > mGitProjectsList.size())
    throw std::runtime_error("Invalid index");

  return mGitProjectsList.at(index.row());
}

void GitProjectsModel::setProjectsList(GitProjectsList && list)
{
  mGitProjectsList = std::move(list);
  Q_FOREACH(GitProject * proj, mGitProjectsList)
    proj->setParent(this);
}

const GitProjectsList & GitProjectsModel::getProjectsList() const
{
  return mGitProjectsList;
}

void GitProjectsModel::addProject(GitProject * project)
{
  project->setParent(this);
  int pos = mGitProjectsList.size();
  beginInsertRows(QModelIndex(), pos, pos);
  mGitProjectsList.append(project);
  endInsertRows();
}

void GitProjectsModel::removeProject(QModelIndex index)
{
  if (!index.isValid() || index.row() < 0 || index.row() > static_cast<int>(mGitProjectsList.size()))
    return;

  beginRemoveRows(QModelIndex(), index.row(), index.row());
  mGitProjectsList.removeAt(index.row());
  endRemoveRows();
}

} // namespace RevistaGit

