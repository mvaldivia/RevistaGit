#include "GitRepositoryStatusModel.h"

#include <Status.h>

namespace RevistaGit {

// ##################################################################
GitRepositoryStatusModel::TEntryStatus ToEntryStatus(Git2Cpp::StatusFlags flags)
{
  if (FLAG_HAS_VALUE(flags, Git2Cpp::StatusFlags::CONFLICTED))
    return GitRepositoryStatusModel::Conflicted;
  else if (FLAG_HAS_VALUE(flags, Git2Cpp::StatusFlags::IGNORED))
    return GitRepositoryStatusModel::Ignored;
  else if (FLAG_HAS_VALUE(flags, Git2Cpp::StatusFlags::WT_UNREADABLE))
    return GitRepositoryStatusModel::Unreadable;
  else if (FLAG_HAS_VALUE(flags, Git2Cpp::StatusFlags::INDEX_NEW) || FLAG_HAS_VALUE(flags, Git2Cpp::StatusFlags::WT_NEW))
    return GitRepositoryStatusModel::New;
  else if (FLAG_HAS_VALUE(flags, Git2Cpp::StatusFlags::INDEX_MODIFIED) || FLAG_HAS_VALUE(flags, Git2Cpp::StatusFlags::WT_MODIFIED))
    return GitRepositoryStatusModel::Modified;
  else if (FLAG_HAS_VALUE(flags, Git2Cpp::StatusFlags::INDEX_DELETED) || FLAG_HAS_VALUE(flags, Git2Cpp::StatusFlags::WT_DELETED))
    return GitRepositoryStatusModel::Deleted;
  else if (FLAG_HAS_VALUE(flags, Git2Cpp::StatusFlags::INDEX_RENAMED) || FLAG_HAS_VALUE(flags, Git2Cpp::StatusFlags::WT_RENAMED))
    return GitRepositoryStatusModel::Renamed;
  else if (FLAG_HAS_VALUE(flags, Git2Cpp::StatusFlags::INDEX_TYPECHANGE) || FLAG_HAS_VALUE(flags, Git2Cpp::StatusFlags::WT_TYPECHANGE))
    return GitRepositoryStatusModel::TypeChange;
  else
    return GitRepositoryStatusModel::Current;
}

// ##################################################################
GitRepositoryStatusModel::GitRepositoryStatusModel(QObject *parent)
  : QAbstractListModel(parent)
  , mType(Type::INDEX)
{
}

GitRepositoryStatusModel::~GitRepositoryStatusModel()
{

}

const Git2Cpp::Repository *GitRepositoryStatusModel::repository()
{
  throw std::runtime_error("Private method");
  return nullptr;
}

void GitRepositoryStatusModel::setRepository(const Git2Cpp::Repository *repository)
{
  Git2Cpp::StatusOptions opts = \
      Git2Cpp::StatusOptions::INCLUDE_UNTRACKED | \
      Git2Cpp::StatusOptions::RENAMES_FROM_REWRITES;

  Git2Cpp::StatusShow show;
  if (mType == Type::INDEX)
  {
    show = Git2Cpp::StatusShow::INDEX_ONLY;
    opts |= Git2Cpp::StatusOptions::RENAMES_HEAD_TO_INDEX;
  }
  else
  {
    show = Git2Cpp::StatusShow::WORKDIR_ONLY;
    opts |= Git2Cpp::StatusOptions::RENAMES_INDEX_TO_WORKDIR;
  }

  beginResetModel();
  mFilesStatus.clear();
  Git2Cpp::Status::list(repository, show, opts, [this](std::string filename, Git2Cpp::StatusFlags flags) -> int
  {
    mFilesStatus.push_back(std::make_pair(QString::fromStdString(filename), ToEntryStatus(flags)));
    return 0;
  });
  endResetModel();
}

GitRepositoryStatusModel::Type GitRepositoryStatusModel::type() const
{
  return mType;
}

void GitRepositoryStatusModel::setType(GitRepositoryStatusModel::Type type)
{
  mType = type;
}

int GitRepositoryStatusModel::rowCount(const QModelIndex &parent) const
{
  // For list models only the root node (an invalid parent) should return the list's size. For all
  // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
  if (parent.isValid())
    return 0;

  return static_cast<int>(mFilesStatus.size());
}

QVariant GitRepositoryStatusModel::data(const QModelIndex &index, int role) const
{
  if (!index.isValid())
    return QVariant();

  switch (role)
  {
  case Qt::DisplayRole:
  case FileName:
    return mFilesStatus.at(index.row()).first;
  case FileStatus:
    return mFilesStatus.at(index.row()).second;
  default:
    return QVariant();
  }

}

QHash<int, QByteArray> GitRepositoryStatusModel::roleNames() const
{
  QHash<int, QByteArray> roles;
  roles[FileName] = "fileName";
  roles[FileStatus] = "fileStatus";
  return roles;
}

} // namespace RevistaGit
