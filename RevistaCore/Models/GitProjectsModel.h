#ifndef REVISTAGIT_GITPROJECTSMODEL_H
#define REVISTAGIT_GITPROJECTSMODEL_H

#include <QAbstractListModel>
#include "Core/RevistaGit_global.h"

namespace RevistaGit {

class GitProject;

class GitProjectsModel : public QAbstractListModel
{
  Q_OBJECT

public:
  GitProjectsModel(QObject *parent = nullptr);
  ~GitProjectsModel();

  enum GitProjectsRoles {
    PathRole = Qt::UserRole + 1,
    NameRole
  };

  // QAbstractItemModel interface
  // read only overrides
  int rowCount(const QModelIndex &parent) const;
  QVariant data(const QModelIndex &index, int role) const;
  QHash<int, QByteArray> roleNames() const override;

  // edit model
  void setProjectsList(GitProjectsList && list);
  const GitProjectsList & getProjectsList() const;

  Q_INVOKABLE GitProject *getProject(const QModelIndex &index);
  void addProject(GitProject *project);
  void removeProject(QModelIndex index);

private:
  GitProjectsList mGitProjectsList;


  // QAbstractItemModel interface
public:
  QModelIndex index(int row, int column, const QModelIndex &parent) const override;
};

} // namespace RevistaGit

#endif // REVISTAGIT_GITPROJECTSMODEL_H
