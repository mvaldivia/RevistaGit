#include "GitCommitTreeModelLogWorker.h"
#include <RevWalk.h>
#include <Repository.h>
#include <Oid.h>
#include <Commit.h>
#include <Tree.h>

#include <QDebug>

#include "GitCommitTreeModel.h"

namespace RevistaGit {

GitCommitTreeModelLogWorker::GitCommitTreeModelLogWorker(const Git2Cpp::Commit *commit, TMapItems && mapItems)
  : QThread(nullptr)
  , mWalker(new Git2Cpp::RevWalk(commit))
  , mMapItems(std::move(mapItems))
{

}

GitCommitTreeModelLogWorker::~GitCommitTreeModelLogWorker()
{
  delete mWalker;
}

void GitCommitTreeModelLogWorker::run()
{
  qDebug() << "Worker running";
  Git2Cpp::Repository repo = mWalker->getRepository();
  Git2Cpp::Oid commitId;
  std::string lastCommitLog;
  while (!(commitId = mWalker->next()).isNull() && mMapItems.size() && !isInterruptionRequested())
  {
    Git2Cpp::Commit commit = repo.lookup<Git2Cpp::Commit>(commitId);
    Git2Cpp::Tree tree = commit.getTree();
    TMapItems currentMap;

    tree.walk([this, &currentMap](std::string /*root*/, std::string /*name*/, Git2Cpp::Oid oid, bool /*isTree*/) -> int
    {
      if (isInterruptionRequested() || !mMapItems.size())
        return -1;

      currentMap[oid] = nullptr;
      return 0;
    });

    TMapItems::iterator it = mMapItems.begin();
    while(it != mMapItems.end() && !isInterruptionRequested())
    {
      TMapItems::iterator curIt = currentMap.find(it->first);
      if (curIt == currentMap.end())
      {
        TreeItem * i = it->second;
        i->mLog = lastCommitLog;
        emit logUpdated(i->mIndex);
        it = mMapItems.erase(it);
      }
      else
        it++;
    }

    lastCommitLog = commit.getDateTime() + " " + commit.getSummary();

  }
  qDebug() << "Worker done";
}

} // namespace RevistaGit
