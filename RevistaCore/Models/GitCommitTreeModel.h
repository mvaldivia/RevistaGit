#ifndef REVISTAGIT_GITCOMMITTREEMODEL_H
#define REVISTAGIT_GITCOMMITTREEMODEL_H

#include <QAbstractItemModel>
#include <QThread>
#include <memory>
#include <map>

#include "RevistaCore.h"
#include <Oid.h>

namespace Git2Cpp {
class Commit;
class Oid;
class RevWalk;
}

namespace RevistaGit {


class GitCommitTreeModelLogWorker;
// ###########################################################################
class TreeItem
{
public:
  explicit TreeItem(const std::string &name = "", Git2Cpp::Oid *oid = nullptr, bool isTree = false, TreeItem *parent = nullptr);
  ~TreeItem();

  void appendChild(TreeItem * child);
  int count() const;

  std::string mName;
  Git2Cpp::Oid *mOid;  
  bool mIsTree;
  Git2Cpp::Oid mCommitId;
  std::string mLog;

  std::vector<TreeItem *> mChildren;
  TreeItem *mParent;
  QModelIndex mIndex;
};

typedef std::map<Git2Cpp::Oid, TreeItem*> TMapItems;

// ###########################################################################
class GitCommitTreeModel : public QAbstractItemModel
{
  Q_OBJECT
  Q_PROPERTY(const Git2Cpp::Repository * repository READ repository WRITE setRepository NOTIFY repositoryChanged)
  Q_PROPERTY(const Git2Cpp::Reference * reference READ reference WRITE setReference NOTIFY referenceChanged)
  Q_PROPERTY(const Git2Cpp::Commit * commit READ commit WRITE setCommit NOTIFY commitChanged)


public:
  explicit GitCommitTreeModel(QObject *parent = 0, const Git2Cpp::Commit * commit = nullptr);
  ~GitCommitTreeModel();

  void setRepository(const Git2Cpp::Repository * repository);
  void loadTree(const Git2Cpp::Repository * repository);

  void setReference(const Git2Cpp::Reference * reference);
  void loadTree(const Git2Cpp::Reference * reference);

  void setCommit(const Git2Cpp::Commit * commit);
  void loadTree(const Git2Cpp::Commit * commit);


  typedef enum
  {
    OidRole = Qt::UserRole + 1,
    FileNameRole,
    LogRole
  } TreeModelRoleT;



  // Header:
  QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

  // Basic functionality:
  QModelIndex index(int row, int column,
                    const QModelIndex &parent = QModelIndex()) const override;
  QModelIndex parent(const QModelIndex &index) const override;

  int rowCount(const QModelIndex &parent = QModelIndex()) const override;
  int columnCount(const QModelIndex &parent = QModelIndex()) const override;

  QHash<int, QByteArray> roleNames() const override;
  QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

signals:
  void repositoryChanged();
  void referenceChanged();
  void commitChanged();

public slots:
  void onLogUpdated(QModelIndex index);

private:
  std::unique_ptr<TreeItem> mRootItem;
  GitCommitTreeModelLogWorker * mLogWorker;

  void destroyWorker();

  const Git2Cpp::Repository * repository();
  const Git2Cpp::Reference * reference();
  const Git2Cpp::Commit * commit();
};

} // namespace RevistaGit

#endif // REVISTAGIT_GITCOMMITTREEMODEL_H
