#include "GitRepositoryBranchesModel.h"
#include <Repository.h>
#include <Reference.h>

namespace RevistaGit {

GitRepositoryBranchesModel::GitRepositoryBranchesModel(QObject *parent)
  : QAbstractListModel(parent)
{
  //mBranches = QVector<Git2Cpp::Reference*>::fromStdVector(repository->getLocalBranches());
}

GitRepositoryBranchesModel::~GitRepositoryBranchesModel()
{
  for (auto branch : mBranches)
    delete branch;
}

int GitRepositoryBranchesModel::rowCount(const QModelIndex &parent) const
{
  // For list models only the root node (an invalid parent) should return the list's size. For all
  // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
  if (parent.isValid())
    return 0;

  return mBranches.size();
}


QHash<int, QByteArray> GitRepositoryBranchesModel::roleNames() const
{
  QHash<int, QByteArray> roles;
  roles[GuidRole] = "branchGuid";
  roles[ShortNameRole] = "branchShortName";
  roles[NameRole] = "branchName";
  return roles;
}

QVariant GitRepositoryBranchesModel::data(const QModelIndex &index, int role) const
{
  if (!index.isValid())
    return QVariant();

  switch (role)
  {
  case Qt::DisplayRole:
  case ShortNameRole:
    return QString::fromStdString(mBranches.at(index.row())->shorthand());
    break;
  case GuidRole:
    return QString::fromStdString(mBranches.at(index.row())->target().toShortString());
    break;
  case NameRole:
    return QString::fromStdString(mBranches.at(index.row())->fullName());
    break;
  default:
    return QVariant();
  }
}

const Git2Cpp::Repository *GitRepositoryBranchesModel::repository() const
{
  throw std::runtime_error("GitRepositoryBranchesModel::repository() is private");
  return nullptr;
}

void GitRepositoryBranchesModel::setRepository(const Git2Cpp::Repository *repo)
{
  beginResetModel();
  for (auto branch : mBranches)
    delete branch;
  mBranches = QVector<Git2Cpp::Reference*>::fromStdVector(repo->getLocalBranches());
  endResetModel();
}

const Git2Cpp::Reference *GitRepositoryBranchesModel::getReference(int index)
{
  if (index >= 0 && index < mBranches.size())
  {
    return mBranches.at(index);
  }
  return nullptr;
}

} // namespace RevistaGit
