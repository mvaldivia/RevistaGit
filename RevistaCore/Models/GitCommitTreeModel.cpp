#include "GitCommitTreeModel.h"
#include "GitCommitTreeModelLogWorker.h"

#include <Commit.h>
#include <Tree.h>
#include <RevWalk.h>
#include <Repository.h>
#include <Reference.h>


namespace RevistaGit {

// ###########################################################################
TreeItem::TreeItem(const std::string &name, Git2Cpp::Oid *oid, bool isTree, TreeItem *parent)
  : mName(name)
  , mOid(oid)
  , mIsTree(isTree)
  , mParent(parent)
{
}

TreeItem::~TreeItem()
{
  if (mOid) delete mOid;
  for (auto child : mChildren)
    delete child;
}

void TreeItem::appendChild(TreeItem * child)
{
  mChildren.push_back(child);
}

int TreeItem::count() const
{
  return static_cast<int>(mChildren.size());
}

// ###########################################################################
GitCommitTreeModel::GitCommitTreeModel(QObject *parent, const Git2Cpp::Commit *commit)
  : QAbstractItemModel(parent)
  , mRootItem(nullptr)
  , mLogWorker(nullptr)
{
  if (commit)
    loadTree(commit);
}

GitCommitTreeModel::~GitCommitTreeModel()
{
  destroyWorker();
}

const Git2Cpp::Repository * GitCommitTreeModel::repository()
{
  throw std::runtime_error("Private method");
  return nullptr;
}

void GitCommitTreeModel::setRepository(const Git2Cpp::Repository *repository)
{
  loadTree(repository);
}

void GitCommitTreeModel::loadTree(const Git2Cpp::Repository *repository)
{
  if (!repository) {
    loadTree((const Git2Cpp::Commit*) nullptr);
    return;
  }
  Git2Cpp::Reference head = repository->getHead();
  loadTree(&head);
}

const Git2Cpp::Reference * GitCommitTreeModel::reference()
{
  throw std::runtime_error("Private method");
  return nullptr;
}

void GitCommitTreeModel::setReference(const Git2Cpp::Reference *reference)
{
  loadTree(reference);
}

void GitCommitTreeModel::loadTree(const Git2Cpp::Reference * reference)
{
  if (!reference) {
    loadTree((const Git2Cpp::Commit*) nullptr);
    return;
  }
  Git2Cpp::Repository repo = reference->getRepository();
  Git2Cpp::Commit commit = repo.lookup<Git2Cpp::Commit>(reference->target());
  loadTree(&commit);
}

const Git2Cpp::Commit * GitCommitTreeModel::commit()
{
  throw std::runtime_error("Private method");
  return nullptr;
}

void GitCommitTreeModel::setCommit(const Git2Cpp::Commit *commit)
{
  loadTree(commit);
}

void GitCommitTreeModel::loadTree(const Git2Cpp::Commit *commit)
{
  beginResetModel();
  mRootItem.reset(new TreeItem());
  destroyWorker();
  if (!commit)
  {
    endResetModel();
    return;
  }

  TMapItems mapItems;
  Git2Cpp::Tree tree = commit->getTree();
  tree.walk([this, &mapItems](std::string root, std::string name, Git2Cpp::Oid oid, bool isTree) -> int
  {
    if (root.empty())
    {
      TreeItem *item = new TreeItem(name, new Git2Cpp::Oid(oid), isTree, mRootItem.get());
      item->mIndex = index(0, 0);
      mapItems[*(item->mOid)] = item;
      mRootItem->appendChild(item);
    }
    else
    {
      QString path = QString::fromStdString(root);
      QStringList splitted = path.split("/");
      TreeItem * parent = mRootItem.get();
      for (auto part :  splitted)
      {
        if (part.isEmpty())
          break;

        for (auto child : parent->mChildren)
        {
          if (part == QString::fromStdString(child->mName))
          {
            parent = child;
            break;
          }
        }
      }
      if (parent != mRootItem.get())
      {
        TreeItem *item = new TreeItem(name, new Git2Cpp::Oid(oid), isTree, parent);
        item->mIndex = index(parent->mChildren.size(), 0, parent->mIndex);
        mapItems[*(item->mOid)] = item;
        parent->appendChild(item);
      }
    }
    return 0;
  });
  endResetModel();

  mLogWorker = new GitCommitTreeModelLogWorker(commit, std::move(mapItems));
  connect(mLogWorker, &GitCommitTreeModelLogWorker::logUpdated, this, &GitCommitTreeModel::onLogUpdated);
  mLogWorker->start();

}

QVariant GitCommitTreeModel::headerData(int section, Qt::Orientation orientation, int role) const
{
  if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
  {
    if (section == 0)
      return "Name";
    else if (section == 1)
      return "Oid";
    else if (section == 2)
      return "Log";
  }

  return QVariant();
}

QModelIndex GitCommitTreeModel::index(int row, int column, const QModelIndex &parent) const
{
  if (!hasIndex(row, column, parent))
    return QModelIndex();

  TreeItem *parentItem;

  if (!parent.isValid())
    parentItem = mRootItem.get();
  else
    parentItem = static_cast<TreeItem *>(parent.internalPointer());

  size_t pos = static_cast<size_t>(row);
  if (parentItem->mChildren.size() > pos)
  {
    TreeItem * child = parentItem->mChildren.at(pos);
    return createIndex(row, column, child);
  }
  else
    return QModelIndex();
}

QModelIndex GitCommitTreeModel::parent(const QModelIndex &index) const
{
  if (!index.isValid())
    return QModelIndex();

  TreeItem *childItem = static_cast<TreeItem*>(index.internalPointer());
  TreeItem *parentItem = childItem->mParent;

  if (parentItem == mRootItem.get())
    return QModelIndex();

  int row = 0;
  for (size_t i = 0; i < parentItem->mChildren.size(); i++)
  {
    if (parentItem->mChildren[i] == childItem)
    {
      row = static_cast<int>(i);
      break;
    }
  }
  return createIndex(row, 0, parentItem);
}

int GitCommitTreeModel::rowCount(const QModelIndex &parent) const
{
  TreeItem *parentItem;
  if (parent.column() > 0)
    return 0;

  if (!parent.isValid())
    parentItem = mRootItem.get();
  else
    parentItem = static_cast<TreeItem*>(parent.internalPointer());

  if (parentItem)
    return static_cast<int>(parentItem->mChildren.size());
  else
    return 0;
}

int GitCommitTreeModel::columnCount(const QModelIndex & /*parent*/) const
{
  return 3; // name, oid, log
}

QHash<int, QByteArray> GitCommitTreeModel::roleNames() const
{
  QHash<int, QByteArray> roles;
  roles[OidRole] = "oid";
  roles[FileNameRole] = "fileName";
  roles[LogRole] = "log";
  return roles;
}

QVariant GitCommitTreeModel::data(const QModelIndex &index, int role) const
{
  if (!index.isValid())
    return QVariant();

  TreeItem *item = static_cast<TreeItem*>(index.internalPointer());

  std::string res;
  if (role == Qt::DisplayRole)
  {
    switch (index.column()) {
    case 0:
      res = item->mName;
      break;
    case 1:
      res = item->mOid ? item->mOid->toShortString() : "";
      break;
    case 2:
      res = item->mLog;
      break;
    }
  }
  else if (role == FileNameRole)
  {
    res = item->mName;
  }
  else if (role == OidRole)
  {
    res = item->mOid ? item->mOid->toString() : "";
  }
  else if (role == LogRole)
  {
    res = item->mLog;
  }
  else
    return QVariant();
  return QString::fromStdString(res);
}

void GitCommitTreeModel::onLogUpdated(QModelIndex index)
{
  QVector<int> v;
  v << Qt::DisplayRole;
  emit dataChanged(index, index, v);
}

void GitCommitTreeModel::destroyWorker()
{
  if (mLogWorker)
  {
    mLogWorker->requestInterruption();
    mLogWorker->quit();
    mLogWorker->wait();
    delete mLogWorker;
    mLogWorker = nullptr;
  }
}

} // namespace RevistaGit
