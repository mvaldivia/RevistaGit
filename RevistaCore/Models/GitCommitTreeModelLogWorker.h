#ifndef GITCOMMITTREEMODELLOGWORKER_H
#define GITCOMMITTREEMODELLOGWORKER_H

#include <QThread>
#include <QModelIndex>

namespace Git2Cpp {
  class Commit;
  class RevWalk;
  class Oid;
}

namespace RevistaGit {

class TreeItem;
typedef std::map<Git2Cpp::Oid, TreeItem*> TMapItems;


class GitCommitTreeModelLogWorker : public QThread
{
  Q_OBJECT

public:
  GitCommitTreeModelLogWorker(const Git2Cpp::Commit *commit, TMapItems &&mapItems);
  ~GitCommitTreeModelLogWorker();

signals:
  void logUpdated(QModelIndex index);

  // QThread interface
protected:
  void run() Q_DECL_OVERRIDE;

private:
  Git2Cpp::RevWalk *mWalker;
  TMapItems mMapItems;
};

} // namespace RevistaGit

#endif // GITCOMMITTREEMODELLOGWORKER_H
