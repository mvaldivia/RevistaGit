#include "GitLogModel.h"

#include <sstream>

#include <Repository.h>
#include <Reference.h>
#include <RevWalk.h>
#include <Commit.h>

#include <QDebug>

namespace RevistaGit {

GitLogModel::GitLogModel(const Git2Cpp::Repository *repository, QObject *parent)
  : QAbstractListModel(parent)
  , mRepository(repository)
  , mRevWalk(nullptr)
  , mPageNbItems(20)
  , mCanFetchMore(false)
{
  if (mRepository)
  {
    mRevWalk.reset(new Git2Cpp::RevWalk(mRepository));
    mCanFetchMore = true;
  }
}

GitLogModel::~GitLogModel()
{

}

int GitLogModel::rowCount(const QModelIndex &parent) const
{
  // For list models only the root node (an invalid parent) should return the list's size. For all
  // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
  if (parent.isValid())
    return 0;

  return static_cast<int>(mCommitList.size());
}

QHash<int, QByteArray> GitLogModel::roleNames() const
{
  QHash<int, QByteArray> roles;
  roles[GuidRole] = "commitGuid";
  roles[TitleRole] = "commitTitle";
  roles[AuthorRole] = "commitAuthor";
  roles[DateRole] = "commitDate";
  return roles;
}

QVariant GitLogModel::data(const QModelIndex &index, int role) const
{
  if (!index.isValid())
    return QVariant();

  const Git2Cpp::Commit & commit = mCommitList.at(index.row());

  std::string res;
  switch (role) {
  case Qt::DisplayRole:
  {
    std::ostringstream oss;
    oss << commit.getId().toShortString() << " : " << commit.getSummary() << std::endl;
    oss << " (" << commit.getAuthor() << ")" << " (" << commit.getCommitter() << ")" << std::endl;
    res = oss.str();
    break;
  }
  case GuidRole:
    res = commit.getId().toShortString();
    break;
  case TitleRole:
    res = commit.getSummary();
    break;
  case AuthorRole:
    res = commit.getAuthor().toString();
    break;
  case DateRole:
    res = commit.getDateTime();
    break;
  default:
    break;
  }

  if (res.empty())
    return QVariant();
  else
    return QString::fromStdString(res);
}

void GitLogModel::setStart(const Git2Cpp::Oid &oid)
{
  beginResetModel();
  mRevWalk->setStart(oid);
  mCommitList.clear();
  mCanFetchMore = true;
  endResetModel();
}

void GitLogModel::setStart(const Git2Cpp::Reference *ref)
{
  setStart(ref->target());
}

const Git2Cpp::Repository *GitLogModel::repository() const
{
  return mRepository;
}

void GitLogModel::setRepository(const Git2Cpp::Repository *repository)
{
  mRepository = repository;
  if (mRepository)
  {
    beginResetModel();
    mRevWalk.reset(new Git2Cpp::RevWalk(mRepository));
    mCommitList.clear();
    mCanFetchMore = true;
    endResetModel();

    emit repositoryChanged();
  }
}

bool GitLogModel::canFetchMore(const QModelIndex & /*parent*/) const
{
  return mCanFetchMore;
}

void GitLogModel::fetchMore(const QModelIndex & /*parent*/)
{
  if (!mCanFetchMore) return;

  Git2Cpp::Oid commitId;
  int cnt = 0;

  qDebug() << "Calling fetchmore";

  std::vector<Git2Cpp::Commit> commitList;

  do
  {
    commitId = mRevWalk->next();
    if (!commitId.isNull())
    {
      Git2Cpp::Commit commit = mRepository->lookup<Git2Cpp::Commit>(commitId);
      //mCommitList.push_back(std::move(commit));
      commitList.push_back(std::move(commit));
      cnt++;
    }
  } while (!commitId.isNull() && cnt < mPageNbItems);

  if (cnt > 0)
  {
    int pos = static_cast<int>(mCommitList.size());
    beginInsertRows(QModelIndex(), pos, pos + cnt - 1);
    mCommitList.insert(mCommitList.end(),
                       std::make_move_iterator(commitList.begin()),
                       std::make_move_iterator(commitList.end())
                       );
    endInsertRows();
  }

  qDebug() << "Fetched " << cnt;

  mCanFetchMore = !commitId.isNull();
}

} // namespace RevistaGit
