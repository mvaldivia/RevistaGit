import QtQuick 2.5
import QtQml.Models 2.2

Rectangle {
    property alias model: visualModel.model
    signal projectSelected(var modelIndex)

    color: "white"

    VisualDataModel {
        id: visualModel
        model: Revista.projectsModel
        delegate: ProjectsListViewDelegate {
            projectName: name
            projectPath: path

            anchors {
                left: parent.left
                right: parent.right
            }


            MouseArea {
                anchors.fill: parent
                onDoubleClicked: {
                    console.log("item double clicked", index, listView.model.modelIndex(index).data);
                    projectSelected(listView.model.modelIndex(index));
                    listView.currentIndex = index
                }
            }
        }
    }

    ListView {
        id: listView
        anchors.fill: parent
        model: visualModel
    }

}
