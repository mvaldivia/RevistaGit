import QtQuick 2.4
import org.revistagit.models 1.0


Rectangle {
    property var repository
    onRepositoryChanged: {
        statusModel.repository = repository;
    }

    property alias type: statusModel.type

    function statusToState(status) {
        switch (status) {
        case GitRepositoryStatusModel.New:
            if (statusModel.type == GitRepositoryStatusModel.INDEX) {
                return "fileNew";
            } else {
                return "fileUntracked";
            }
        case GitRepositoryStatusModel.Modified:
            return "fileModified";
        case GitRepositoryStatusModel.Deleted:
            return "fileDeleted";
        case GitRepositoryStatusModel.Renamed:
        case GitRepositoryStatusModel.TypeChange:
            return "fileRenamed";
        case GitRepositoryStatusModel.Ignored:
            return "fileIgnored";
        case GitRepositoryStatusModel.Conflicted:
            return "fileConflicted";
        default:
            return "";
        }
    }

    GitRepositoryStatusModel {
        id: statusModel
    }

    ListView {
        id: listView
        anchors.fill: parent
        model: statusModel
        clip: true
        delegate: StatusListViewDelegate {
            width: parent.width
            text1.text: fileName
            state: statusToState(fileStatus)
        }
    }
}

