import QtQuick 2.0

Rectangle {
    property alias projectName: projectName.text
    property alias projectPath: projectPath.text
    property bool isCurrent

    id: myitem
    height: 40
    color: isCurrent ? "grey" : "transparent"

    Column {
        id: column
        anchors.margins: 4
        anchors.fill: parent
        spacing: 2

        Text {
            id: projectName
            width: column.width
            font.bold: true
            elide: Text.ElideLeft
        }

        Text {
            id: projectPath
            font.pixelSize: 12
            width: column.width
            elide: Text.ElideLeft
        }
    }
}
