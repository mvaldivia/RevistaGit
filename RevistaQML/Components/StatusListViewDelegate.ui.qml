import QtQuick 2.4

Item {
    property alias text1: text1
    height: 20

    Row {
        id: row
        anchors.fill: parent
        spacing: 10

        Rectangle {
            id: rectangle
            width: 20
            height: 20
            color: "#ffffff"

            Text {
                id: text2
                text: qsTr("")
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                anchors.fill: parent
                font.pixelSize: 12
            }
        }

        Text {
            id: text1
            height: 20
            text: qsTr("/path/to/file")
            verticalAlignment: Text.AlignVCenter
            elide: Text.ElideMiddle
            font.pixelSize: 12
        }
    }
    states: [
        State {
            name: "fileNew"

            PropertyChanges {
                target: rectangle
                color: "#46f964"
            }

            PropertyChanges {
                target: text2
                text: qsTr("N")
            }
        },
        State {
            name: "fileModified"

            PropertyChanges {
                target: rectangle
                color: "#0d77fb"
            }

            PropertyChanges {
                target: text2
                text: qsTr("M")
            }
        },
        State {
            name: "fileDeleted"

            PropertyChanges {
                target: rectangle
                color: "#110909"
            }

            PropertyChanges {
                target: text1
                color: "#756b6b"
                font.strikeout: true
            }

            PropertyChanges {
                target: text2
                text: qsTr("D")
            }
        },
        State {
            name: "fileRenamed"

            PropertyChanges {
                target: rectangle
                color: "#6e07ef"
            }

            PropertyChanges {
                target: text2
                text: qsTr("RN")
            }
        },
        State {
            name: "fileIgnored"

            PropertyChanges {
                target: rectangle
                color: "#cccaca"
            }

            PropertyChanges {
                target: text1
                color: "#989090"
                font.italic: true
            }

            PropertyChanges {
                target: text2
                text: qsTr("I")
            }
        },
        State {
            name: "fileConflicted"

            PropertyChanges {
                target: rectangle
                color: "#fb0404"
            }

            PropertyChanges {
                target: text1
                font.bold: true
            }

            PropertyChanges {
                target: text2
                text: qsTr("C")
            }
        },
        State {
            name: "fileUntracked"

            PropertyChanges {
                target: rectangle
                color: "#fd8f06"
            }

            PropertyChanges {
                target: text2
                text: qsTr("U")
            }
        }
    ]
}
