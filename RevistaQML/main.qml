import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import org.revistagit.models 1.0

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: qsTr("Revista Git")

    menuBar: MenuBar {
        Menu {
            title: qsTr("File")
            MenuItem {
                text: qsTr("&Open")
                onTriggered: console.log("Open action triggered");
            }
            MenuItem {
                text: qsTr("Exit")
                onTriggered: Qt.quit();
            }
        }
    }


    MainForm {
        anchors.fill: parent

        projectsListView.model: Revista.projectsModel
        projectsListView.onProjectSelected: {
            var proj = Revista.projectsModel.getProject(modelIndex);
            console.log("--- selected ", modelIndex, proj, proj.name);
            //var proj = Revista.projectsModel.getProject(modelIndex);
            //projectView.commitTreeModel = proj.
            // This is available in all editors.))
            proj.open();
            var repo = proj.repository
            console.log("repo", repo);
            projectView.repository = repo;
            //logModel.repository = repo;
            console.log("repo", repo);
        }
    }

    GitLogModel {
        id: logModel
    }

    MessageDialog {
        id: messageDialog
        title: qsTr("May I have your attention, please?")

        function show(caption) {
            messageDialog.text = caption;
            messageDialog.open();
        }
    }
}
