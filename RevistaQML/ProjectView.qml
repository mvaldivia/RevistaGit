import QtQuick 2.5
//import org.revistagit.models 1.0

ProjectViewForm {
    property var repository
    property var commitTreeModel: null //revBrowser.model


    revLog.repository: repository
    revBrowser.repository: repository
    repoStatus.repository: repository
}
