import QtQuick 2.5
import "Components"
import "Panes"
import QtQuick.Controls 1.4

Item {
    width: 640
    height: 480
    property alias splitView: splitView
    property alias projectsListView: projectsListView
    property alias projectView: projectView

    SplitView {
        id: splitView
        anchors.fill: parent
        orientation: Qt.Horizontal

        ProjectsListView {
            id: projectsListView
            x: 44
            y: 41
            width: 156
            height: 193
        }

        ProjectView {
            id: projectView
        }
    }
}
