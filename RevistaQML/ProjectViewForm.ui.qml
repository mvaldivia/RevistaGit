import QtQuick 2.5
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import "Panes"


//import org.revistagit.models 1.0
Item {
    id: item1
    width: 400
    height: 400
    property alias revLog: revLog
    property alias revBrowser: revBrowser
    property alias repoStatus: repoStatus

    property var gitProject: null
    property var repository: null

    ColumnLayout
    {
        anchors.fill: parent
        spacing: 0
    TabBar {
        id: tabBar
        width: parent.width
        currentIndex: 1
        Layout.fillWidth: true

        TabButton {
            id: tabButton
            text: qsTr("Files")
        }

        TabButton {
            id: tabButton1
            text: qsTr("Log")
        }

        TabButton {
            id: tabButton2
            text: qsTr("Status")
        }
    }

    StackLayout {
        width: parent.width
        currentIndex: tabBar.currentIndex
        Layout.fillWidth: true
        Layout.fillHeight: true

        RevBrowser {
            id: revBrowser
        }
        RevLog {
            id: revLog
        }
        RepoStatus {
            id: repoStatus
        }
    }
    }
}
