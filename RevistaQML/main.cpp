#include <QCoreApplication>
#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include <Core/ApplicationContext.h>
#include <Core/GitProject.h>

#include <Models/GitLogModel.h>
#include <Models/GitRepositoryBranchesModel.h>
#include <Models/GitCommitTreeModel.h>
#include <Models/GitRepositoryStatusModel.h>

//using RevistaGit;
#include <RevistaCore.h>

/*
// In Linux to increase number of files descriptors for QFileSystemWatcher
#include <sys/time.h>
#include <sys/resource.h>

  struct rlimit myLimits;
  getrlimit(RLIMIT_NOFILE, &myLimits);

*/


int main(int argc, char *argv[])
{


  QCoreApplication::setOrganizationName("mva");
  QCoreApplication::setApplicationName("RevistaGit");

  // Register QML types
  qmlRegisterInterface<RevistaGit::GitProject>("GitProject");
  qmlRegisterType<RevistaGit::GitLogModel>("org.revistagit.models", 1, 0, "GitLogModel");
  qmlRegisterType<RevistaGit::GitRepositoryBranchesModel>("org.revistagit.models", 1, 0, "GitRepositoryBranchesModel");
  qmlRegisterType<RevistaGit::GitCommitTreeModel>("org.revistagit.models", 1, 0, "GitCommitTreeModel");
  qmlRegisterType<RevistaGit::GitRepositoryStatusModel>("org.revistagit.models", 1, 0, "GitRepositoryStatusModel");

  // Register QVariant types
  qRegisterMetaType<const Git2Cpp::Repository*>();
  qRegisterMetaType<const Git2Cpp::Reference*>();
  qRegisterMetaType<const Git2Cpp::Commit*>();

  QApplication app(argc, argv);
  QQmlApplicationEngine engine;

  RevistaGit::ApplicationContext appContext;
  engine.rootContext()->setContextProperty("Revista", &appContext);

  engine.load(QUrl(QStringLiteral("qrc:/main.qml")));



  return app.exec();
}
