TEMPLATE = app

QT += qml quick widgets

CONFIG += c++11

SOURCES += main.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../RevistaCore/release/ -lRevistaCore
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../RevistaCore/debug/ -lRevistaCore
else:unix: LIBS += -L$$OUT_PWD/../RevistaCore/ -lRevistaCore

INCLUDEPATH += $$PWD/../RevistaCore
DEPENDPATH += $$PWD/../RevistaCore

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../RevistaCore/release/libRevistaCore.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../RevistaCore/debug/libRevistaCore.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../RevistaCore/release/RevistaCore.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../RevistaCore/debug/RevistaCore.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../RevistaCore/libRevistaCore.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../Git2Cpp/release/ -lGit2Cpp
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../Git2Cpp/debug/ -lGit2Cpp
else:unix: LIBS += -L$$OUT_PWD/../Git2Cpp/ -lGit2Cpp

INCLUDEPATH += $$PWD/../Git2Cpp
DEPENDPATH += $$PWD/../Git2Cpp

