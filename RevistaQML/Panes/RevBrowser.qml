import QtQuick 2.4
import org.revistagit.models 1.0


RevBrowserForm {
    property var repository
    onRepositoryChanged: {
        if (repository) {
            treeModel.repository = repository
        }
    }

    GitCommitTreeModel {
        id: treeModel
    }
    treeView.model: treeModel

}
