import QtQuick 2.4
import QtQuick.Layouts 1.3
import "../Components"

Item {
    width: 400
    height: 400
    property alias listViewStatusWorkDir: listViewStatusWorkDir
    property alias listViewStatusIndex: listViewStatusIndex

    ColumnLayout {
        id: columnLayout
        anchors.fill: parent

        StatusListView {
            id: listViewStatusIndex
            Layout.fillHeight: true
            Layout.fillWidth: true
        }

        StatusListView {
            id: listViewStatusWorkDir
            Layout.fillHeight: true
            Layout.fillWidth: true
        }
    }
}
