import QtQuick 2.4
import org.revistagit.models 1.0

RevLogForm {
    id: logForm
    property var repository
    onRepositoryChanged: {
        if (repository) {
            logModel.repository = repository
            branchModel.repository = repository
        }
    }

    GitLogModel {
        id: logModel
    }

    GitRepositoryBranchesModel {
        id: branchModel
        onModelReset: comboBox.currentIndex = 0
    }

    listView.model: logModel
    comboBox {
        model: branchModel
        textRole: "branchShortName"
        onActivated: {
            logModel.setStart(branchModel.getReference(index));
        }
    }
    Component.onCompleted: {
        console.log("completed", logModel)//, logModel.namea, logModel.repository)
    }
}
