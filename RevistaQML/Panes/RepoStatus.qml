import QtQuick 2.4
import org.revistagit.models 1.0

RepoStatusForm {
    property var repository
    function setRepository(repo) {
        console.log("=====> RepoStatusForm setRepository", repository);
        if (repository) {
            //statusIndexModel.repository = repository
            //statusWorkDirModel.repository = repository
            listViewStatusIndex.repository = repository
            listViewStatusWorkDir.repository = repository
        }
    }

    onRepositoryChanged: {
        setRepository(repository);
    }

    /*GitRepositoryStatusModel {
        id: statusIndexModel
        type: GitRepositoryStatusModel.INDEX

    }
    listViewStatusIndex.model: statusIndexModel
    */
    listViewStatusIndex.type: GitRepositoryStatusModel.INDEX
    /*GitRepositoryStatusModel {
        id: statusWorkDirModel
        type: GitRepositoryStatusModel.WORK_DIR
    }
    listViewStatusWorkDir.model: statusWorkDirModel
    */
    listViewStatusWorkDir.type: GitRepositoryStatusModel.WORK_DIR

    onVisibleChanged: {
        console.log("--- visible", visible);
        if (visible) {
            setRepository(repository);
        }
    }
}
