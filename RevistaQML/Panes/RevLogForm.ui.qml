import QtQuick 2.4
import QtQuick.Extras 1.4
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3

Rectangle {
    id: item1
    anchors.fill: parent
    property alias listView: listView
    property alias comboBox: comboBox

    ColumnLayout {
        spacing: 2
        anchors.fill: parent
        RowLayout {
            Layout.fillWidth: true

            Text {
                id: text1
                text: qsTr("Branche")
                horizontalAlignment: Text.AlignRight
                Layout.fillWidth: true
                font.pixelSize: 12
            }

            ComboBox {
                id: comboBox
                textRole: "branchShortName"
                model: Repository.branchModel
            }
        }

        ListView {
            id: listView
            Layout.fillWidth: true
            Layout.fillHeight: true
            model: Repository.logModel
            clip: true
            spacing: 5

            ScrollBar.vertical: ScrollBar {}

            delegate: Item {
                id: itemDelegate
                height: columnLayout.implicitHeight
                width: parent.width


                ColumnLayout {
                    id: columnLayout
                    spacing: 0
                    anchors.fill: parent
                    anchors.margins: 5


                    Text {
                        id: titleId
                        text: commitTitle
                        elide: Text.ElideRight
                        font.bold: true
                        Layout.fillWidth: true
                        font.pixelSize: 12
                    }

                    RowLayout {
                        id: rowLayout
                        Layout.fillHeight: false
                        Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                        Layout.fillWidth: true

                        Text {
                            id: guidId
                            color: "#818181"
                            text: commitGuid
                            Layout.fillWidth: true
                            textFormat: Text.PlainText
                            elide: Text.ElideMiddle
                            font.pixelSize: 12
                        }

                        Text {
                            id: authorId
                            text: commitAuthor
                            //Layout.preferredWidth: 80
                            elide: Text.ElideRight
                            Layout.alignment: Qt.AlignRight
                            font.pixelSize: 12
                        }

                        Text {
                            id: dateId
                            text: commitDate
                            Layout.alignment: Qt.AlignRight
                            font.pixelSize: 12
                        }
                    }

                    Rectangle {
                        color: "transparent"
                        height: 10
                    }
                }
            }
        }
    }
}
