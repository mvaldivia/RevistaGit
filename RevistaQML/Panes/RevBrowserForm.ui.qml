import QtQuick 2.4
import QtQuick.Controls 1.4

Item {
    id: revBrowserTreeView
    width: 400
    height: 400
    property alias treeView: treeView

    TreeView {
        id: treeView
        anchors.fill: parent

        TableViewColumn {
            title: "Name"
            role: "fileName"
            width: 300
        }
        TableViewColumn {
            title: "Log"
            role: "log"
            width: 100
        }
    }
}
