#include <iostream>

#include "Git2Cpp.h"
#include "Repository.h"
#include "Reference.h"
#include "Oid.h"
#include "Object.h"
#include "Commit.h"
#include "Tree.h"
#include <iostream>

#define ECHO(x) std::cout << x << std::endl

int main(int argc, char *argv[])
{
    Git2Cpp::Initialize();

    std::cout << "###############################" << std::endl;
    std::cout << "Testing Git2Cpp : " << Git2Cpp::Description() << std::endl;
    std::cout << "###############################" << std::endl;


    Git2Cpp::Repository repo;

    try
    {
      std::cout << "branch :" << repo.getCurrentBranch() << std::endl;
    } catch(std::exception &ex)
    {
      std::cout << "OK : Caught exception : " << ex.what() << std::endl;
    }

    try
    {
      //repo.open("/home/mario/Documents/Projets/CuteGit_test");
      repo.open("/home/docwario/DEV/CuteGit_test");
      std::cout << "OK : Repo opened on branch : " << repo.getCurrentBranch() << std::endl;
    }
    catch (std::exception &ex)
    {
      std::cout << "KO : Caught exception : " << ex.what() << std::endl;
    }

    std::string headTarget;
    try
    {
      Git2Cpp::Reference head = repo.getHead();
      std::cout << "OK : head loaded " << std::endl;
#define REF_TYPE(x) std::cout << "\t" << #x << " : " << head.x() << std::endl;
      REF_TYPE(isSymbolic);
      REF_TYPE(isBranch);
      REF_TYPE(isNote);
      REF_TYPE(isRemote);
      REF_TYPE(isTag);
      std::cout << std::endl;

#define REF_STR(x) std::cout << "\t" << #x << " : \"" << head.x() << "\"" << std::endl
      REF_STR(shorthand);
      REF_STR(fullName);
      REF_STR(symbolicTarget);

      //Git2Cpp::Oid oid = head->target();
      REF_STR(target);
      headTarget = head.target().toString();
    }
    catch (std::exception &ex)
    {
      std::cout << "KO : could not get head : " << ex.what() << std::endl;
    }

    try
    {
      Git2Cpp::Object obj = repo.lookup<Git2Cpp::Object>(headTarget);
      ECHO("Object type : " << obj.typeStr());


      // convert GitOBject to Git2Cpp::Commit
      Git2Cpp::Commit commit(std::move(obj));

      Git2Cpp::Tree tree = commit.getTree();
      ECHO("Tree entries count : " << tree.size());
      tree.walk([](std::string root, std::string name, Git2Cpp::Oid /*oid*/, bool /*isTree*/)
      {
        ECHO("root: " << root << ", name:" << name );//<< ", oid:" << oid);
        return 0;
      });

      std::function<void(std::string prefix, Git2Cpp::Tree *)> listTreeEntries = [&listTreeEntries](std::string prefix, Git2Cpp::Tree * tree) -> void
      {
        ECHO("listTreeEntries");
        for (size_t i =  0; i < tree->size(); i++)
        {
          const Git2Cpp::Tree::Entry *entry = tree->get(i);
          if (entry->isTree())
          {
            std::string tmp = prefix;
            tmp.append("/");
            tmp.append(entry->name());
            ECHO("Found Tree : " << tmp);

            Git2Cpp::Tree::UPtr subTree(entry->toTree());
            listTreeEntries(tmp, subTree.get());
          }
          else
            ECHO("Entry : " << prefix << "/" << entry->name());
        }
      };

      listTreeEntries("", &tree);


      // Get and display Git2Cpp::Tree from commit
    }
    catch (std::exception &ex)
    {
      std::cout << "KO : could not get object : " << ex.what() << std::endl;
    }

    try
    {
        std::cout << "Listing files at current revision" << std::endl;

    }
    catch (std::exception &ex)
    {
      std::cout << "Caught exception : " << ex.what() << std::endl;
    }

    Git2Cpp::Uninitialize();
    return 0;
}
