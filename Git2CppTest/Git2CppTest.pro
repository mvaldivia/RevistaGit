TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
#CONFIG -= qt
QT += core

SOURCES += main.cpp

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../Git2Cpp/release/ -lGit2Cpp
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../Git2Cpp/debug/ -lGit2Cpp
else:unix: LIBS += -L$$OUT_PWD/../Git2Cpp/ -lGit2Cpp

INCLUDEPATH += $$PWD/../Git2Cpp
DEPENDPATH += $$PWD/../Git2Cpp
