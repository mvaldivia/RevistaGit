#ifndef GIT2CPP_REVWALK_H
#define GIT2CPP_REVWALK_H

#include "StructWrapper.h"
#include "Oid.h"

extern "C" {
struct git_revwalk;
}

namespace Git2Cpp {

class Repository;
class Commit;

class RevWalk : public StructWrapper<git_revwalk>
{
public:
  RevWalk(const Repository *repository);
  RevWalk(const Commit *commit);
  // disable copy
  RevWalk(const RevWalk &) = delete;
  RevWalk & operator=(const RevWalk &) = delete;
  // move
  RevWalk(RevWalk && walker);
  RevWalk & operator=(RevWalk && walker);
  ~RevWalk();

  void setDefaultOptions();
  void setStart(const Oid & oid = Oid());

  Repository getRepository() const;

  Oid next();

private:
  unsigned int mOptSorting;
  int mLimit;
};

} // namespace Git2Cpp

#endif // GIT2CPP_REVWALK_H
