#ifndef GIT2CPP_GITOBJECT_H
#define GIT2CPP_GITOBJECT_H

#include <string>
#include "git2cpp_global.h"
#include "StructWrapper.h"
#include "Exception.h"

extern "C" {
  struct git_object;
}

namespace Git2Cpp {

class Oid;

class GIT2CPPSHARED_EXPORT Object : public StructWrapper<git_object>
{
public:
  enum class Type{
    ANY,
    COMMIT,
    TREE,
    BLOB,
    TAG
  };

  Object(git_object *obj = nullptr);
  // copy
  Object(const Object & obj);
  Object& operator=(const Object & obj);
  // move
  Object(Object && obj);
  Object& operator=(Object && obj);
  ~Object();

  bool operator==(const Object & obj);
  bool operator!=(const Object & obj);

  std::string typeStr() const;
  Type type() const;
  Oid getId() const;

};

} // namespace Git2Cpp

#endif // GIT2CPP_GITOBJECT_H
