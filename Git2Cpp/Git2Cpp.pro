#-------------------------------------------------
#
# Project created by QtCreator 2016-10-03T21:32:51
#
#-------------------------------------------------

QT       -= gui

TARGET = Git2Cpp
TEMPLATE = lib

DEFINES += GIT2CPP_LIBRARY

SOURCES += Git2Cpp.cpp \
    Repository.cpp \
    Reference.cpp \
    Index.cpp \
    Oid.cpp \
    Commit.cpp \
    Tree.cpp \
    Signature.cpp \
    RevWalk.cpp \
    Object.cpp \
    Exception.cpp \
    Status.cpp

HEADERS += Git2Cpp.h\
        git2cpp_global.h \
    Repository.h \
    Reference.h \
    Index.h \
    Oid.h \
    GitDebug.h \
    Commit.h \
    Tree.h \
    Signature.h \
    RevWalk.h \
    Object.h \
    StructWrapper.h \
    Exception.h \
    Status.h \
    EnumsBitMaskOperator.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

win32
{
	LIBS += -L$$PWD/../../libgit2-0.23.4/build/Debug/ -lgit2
	INCLUDEPATH += $$PWD/../../libgit2-0.23.4/include
	DEPENDPATH += $$PWD/../../libgit2-0.23.4/include
}


unix: CONFIG += link_pkgconfig
unix: PKGCONFIG += libgit2
