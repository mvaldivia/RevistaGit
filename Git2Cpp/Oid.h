#ifndef GIT2CPP_OID_H
#define GIT2CPP_OID_H

#include <memory>
#include <string>
#include <iostream>
#include "git2cpp_global.h"

extern "C" {
  struct git_oid;
}

namespace Git2Cpp {

class GIT2CPPSHARED_EXPORT Oid
{
public:
  Oid();
  Oid(const git_oid * src);
  Oid(const std::string & src);
  // copy
  Oid(const Oid & oid);
  Oid & operator=(const Oid & oid);
  // move
  Oid(Oid && oid);
  Oid & operator=(Oid && oid);
  // destructor
  ~Oid();

  bool operator==(const Oid &oid) const;
  bool operator!=(const Oid &oid) const;
  bool operator <(const Oid &oid) const;

  bool isNull() const;

  std::string toShortString() const;
  std::string toString() const;

  const git_oid * operator*() const;
  git_oid * operator*();

private:
  std::unique_ptr<git_oid> mOid;
};

} // namespace Git2Cpp

inline std::ostream & operator<<(std::ostream & os, const Git2Cpp::Oid & oid)
{
  os << oid.toString();
  return os;
}

#endif // GIT2CPP_OID_H
