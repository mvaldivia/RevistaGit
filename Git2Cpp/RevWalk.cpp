#include "RevWalk.h"
#include <git2/revwalk.h>
#include "Repository.h"
#include "Exception.h"
#include "Commit.h"

#define STEAL_VAR(target, var, val) var = target.var; target.var = val

namespace Git2Cpp {

RevWalk::RevWalk(const Repository *repository)
  : StructWrapper<git_revwalk> (nullptr)
  , mOptSorting(0)
  , mLimit(-1)
{
  int err = git_revwalk_new(&mObject, repository->ptr());
  THROW_IF_ERROR(err);

  setDefaultOptions();
  setStart();
}

RevWalk::RevWalk(const Commit *commit)
  : StructWrapper<git_revwalk> (nullptr)
  , mOptSorting(0)
  , mLimit(-1)
{
  Repository repository = commit->getOwner();
  int err = git_revwalk_new(&mObject, repository.ptr());
  THROW_IF_ERROR(err);

  setDefaultOptions();
  setStart();
}

RevWalk::RevWalk(RevWalk && walker)
  : StructWrapper<git_revwalk> (walker.mObject)
  , mOptSorting(walker.mOptSorting)
  , mLimit(-1)
{
  walker.mObject = nullptr;
  walker.mOptSorting = 0;
  walker.mLimit = -1;
}

RevWalk & RevWalk::operator=(RevWalk && walker)
{
  if (this != &walker)
  {
    STEAL_VAR(walker, mObject, nullptr);
    STEAL_VAR(walker, mOptSorting, 0);
    STEAL_VAR(walker, mLimit, -1);
  }
  return *this;
}

RevWalk::~RevWalk()
{
  if (mObject) git_revwalk_free(mObject);
}

void RevWalk::setDefaultOptions()
{
  mOptSorting = GIT_SORT_TIME;// | GIT_SORT_REVERSE;

  git_revwalk_sorting(mObject, mOptSorting);

}

void RevWalk::setStart(const Oid &oid)
{
  int err;
  git_revwalk_reset(mObject);

  if (oid.isNull())
    err = git_revwalk_push_head(mObject);
  else
    err = git_revwalk_push(mObject, *oid);
  THROW_IF_ERROR(err);
}

Repository RevWalk::getRepository() const
{
  return Repository(git_revwalk_repository(mObject));
}

Oid RevWalk::next()
{
  Oid oid;
  /*if (!git_revwalk_next(*oid, mObject))
  {

  }*/
  git_revwalk_next(*oid, mObject);
  return oid;
}

} // namespace Git2Cpp
