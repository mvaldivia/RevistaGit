#include "Commit.h"

extern "C"
{
#include <time.h>
}

#include <git2/object.h>
#include <git2/commit.h>

#include "Exception.h"
#include "Oid.h"
#include "Tree.h"
#include "Repository.h"

#include <type_traits>

namespace Git2Cpp {

Commit::Commit(git_commit *commit)
  : StructWrapper<git_commit>(commit)
{

}

Commit::Commit(Object &&obj)
  : StructWrapper<git_commit>(nullptr)
{
  if (obj.type() == Object::Type::COMMIT)
  {
    mObject = (git_commit*)obj.ptr();
    obj.setPtr(nullptr);
  }
  else
    throw Exception("Invalid Object type");
}

Commit::Commit(Commit && commit)
  : StructWrapper<git_commit> (commit.mObject)
{
  commit.mObject = nullptr;
}

Commit & Commit::operator=(Commit && commit)
{
  if (this != &commit)
  {
    mObject = commit.mObject;
    commit.mObject = nullptr;
  }
  return *this;
}

Commit::~Commit()
{
  if (mObject != nullptr)
    git_commit_free(mObject);
}

Oid Commit::getId() const
{
  return git_commit_id(mObject);
}

Tree Commit::getTree() const
{
  Tree tree;
  int res = git_commit_tree(tree.ptrAddr(), mObject);
  THROW_IF_ERROR(res);
  return tree;
}

Repository Commit::getOwner() const
{
  return Repository(git_commit_owner(mObject));
}

Signature Commit::getAuthor() const
{
  const git_signature * sig = git_commit_author(mObject);
  return { sig->name, sig->email };
}

Signature Commit::getCommitter() const
{
  const git_signature * sig = git_commit_committer(mObject);
  return { sig->name, sig->email };
}

std::string Commit::getBody() const
{
  const char * body = git_commit_body(mObject);
  return (body != nullptr) ? body : "";
}

std::string Commit::getMessage() const
{
  return git_commit_message(mObject);
}

std::string Commit::getSummary() const
{
  return git_commit_summary(mObject);
}

std::string Commit::getDateTime() const
{
  time_t commitTime = git_commit_time(mObject);
  struct tm localTm;
  localtime_r(&commitTime, &localTm);
  char res[12];
  strftime(res, 12, "%F", &localTm);
  return res;
}

} // namespace Git2Cpp
