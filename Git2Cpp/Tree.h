#ifndef GIT2CPP_TREE_H
#define GIT2CPP_TREE_H

#include <map>
#include <string>
#include <memory>
#include <functional>
#include "git2cpp_global.h"
#include "StructWrapper.h"
#include "Oid.h"

extern "C" {
  struct git_tree;
  struct git_tree_entry;
}

namespace Git2Cpp {

class Tree : public StructWrapper<git_tree>
{
public:
  Tree(git_tree * tree = nullptr);
  // copy
  Tree(const Tree &) = delete;
  Tree & operator=(const Tree &) = delete;
  //move
  Tree(Tree && tree);
  Tree & operator=(Tree && tree);
  ~Tree();

  typedef std::unique_ptr<Tree> UPtr;

  size_t size() const;

  typedef std::function<int(std::string root, std::string name, Oid oid, bool isTree)> walkCb;
  void walk(walkCb cb) const;











  class Entry : public StructWrapper<const git_tree_entry>
  {
  public:
    Entry(const git_tree_entry *entry = nullptr, const Tree *tree = nullptr);
    Entry(const Entry & entry);
    Entry(Entry && entry);
    ~Entry();

    Entry & operator=(const Entry &entry);

    bool isTree() const;
    bool isBlob() const;
    std::string name() const;

    Tree::UPtr toTree() const;

  private:
    const Tree * parent;
  };

  typedef std::map<size_t, Entry> EntryMapT;
  EntryMapT mChildren;
  const Entry * get(size_t idx);
  friend UPtr Entry::toTree() const;
};

} // namespace Git2Cpp

#endif // GIT2CPP_TREE_H
