#include "Reference.h"
#include <git2/refs.h>
#include <Exception.h>
#include <Oid.h>
#include <Repository.h>

namespace Git2Cpp {

Reference::Reference(git_reference *ref)
  : StructWrapper<git_reference> (ref)
{
}

// move
Reference::Reference(Reference && reference)
  : StructWrapper<git_reference> (reference.mObject)
{
  // steal done in ctor
  // erase
  reference.mObject = nullptr;
}

Reference & Reference::operator=(Reference && reference)
{
  if (this != &reference)
  {
    // steal
    mObject = reference.mObject;
    // erase
    reference.mObject = nullptr;
  }
  return *this;
}

Reference::~Reference()
{
  if (mObject != nullptr)
  {
    git_reference_free(mObject);
    mObject = nullptr;
  }
}

std::string Reference::shorthand() const
{
  return git_reference_shorthand(mObject);
}

std::string Reference::fullName() const
{
  return git_reference_name(mObject);
}

std::string Reference::symbolicTarget() const
{
  const char *t = git_reference_symbolic_target(mObject);
  if (t == nullptr)
    return "";
  else
    return t;
}

bool Reference::isSymbolic() const
{
  return (git_reference_type(mObject) == GIT_REF_SYMBOLIC);
}

bool Reference::isBranch() const
{
  return git_reference_is_branch(mObject);
}

bool Reference::isNote() const
{
  return git_reference_is_note(mObject);
}

bool Reference::isRemote() const
{
  return git_reference_is_remote(mObject);
}

bool Reference::isTag() const
{
  return git_reference_is_tag(mObject);
}

Oid Reference::target() const
{
  const git_oid *oid = git_reference_target(mObject);
  if (oid != nullptr)
    return Oid(oid);
  else
    THROW_ERROR("No target found");
}

Repository Reference::getRepository() const
{
  return Repository(git_reference_owner(mObject));
}

} // namespace Git2Cpp
