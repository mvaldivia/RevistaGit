#ifndef GIT2CPP_SIGNATURE_H
#define GIT2CPP_SIGNATURE_H

#include <string>
#include <iostream>

namespace Git2Cpp {

struct Signature
{
  std::string name;
  std::string email;

  std::string toString() const;
};

} // namespace Git2Cpp

inline std::ostream & operator<<(std::ostream & os, const Git2Cpp::Signature & oid)
{
  os << oid.toString();
  return os;
}

#endif // GIT2CPP_SIGNATURE_H
