#include "Tree.h"
#include <git2/tree.h>
#include <git2/types.h>

#include "Exception.h"
#define THROW_IF_UNINIT if (mObject == nullptr) throw Exception("Tree not initialized")

extern "C"
{

static int walk_cb(const char *root,
                   const git_tree_entry *entry,
                   void *payload)
{
  Git2Cpp::Tree::walkCb cb =  *(Git2Cpp::Tree::walkCb*) payload;
  //Git2Cpp::Oid oid(git_tree_entry_id(entry));
  cb(root, git_tree_entry_name(entry), Git2Cpp::Oid(git_tree_entry_id(entry)), (git_tree_entry_type(entry) == GIT_OBJ_TREE));
  return 0;
}

} // extern "C"

namespace Git2Cpp {

// ############################################################
// Tree entry
Tree::Entry::Entry(const git_tree_entry *entry, const Tree *tree)
  : StructWrapper<const git_tree_entry> (entry)
  , parent(tree)
{
}

Tree::Entry::Entry(const Entry & entry)
  : StructWrapper<const git_tree_entry> (nullptr)
{
  if (this != &entry)
  {
    mObject = entry.mObject;
    parent = entry.parent;
  }
}

Tree::Entry::Entry(Entry && entry)
  : StructWrapper<const git_tree_entry> (nullptr)
{
  // steal
  mObject = entry.mObject;
  parent = entry.parent;
  // erase
  entry.mObject = nullptr;
  entry.parent = nullptr;
}

Tree::Entry::~Entry()
{
}

Tree::Entry & Tree::Entry::operator=(const Entry &entry)
{
  if (this != &entry)
  {
    mObject = entry.mObject;
    parent = entry.parent;
  }
  return *this;
}

bool Tree::Entry::isTree() const
{
  return (git_tree_entry_type(mObject) == GIT_OBJ_TREE);
}

bool Tree::Entry::isBlob() const
{
  return (git_tree_entry_type(mObject) == GIT_OBJ_BLOB);
}

std::string Tree::Entry::name() const
{
  return git_tree_entry_name(mObject);
}

Tree::UPtr Tree::Entry::toTree() const
{
  git_repository *repo = git_tree_owner(parent->mObject);
  git_object *obj;
  int err = git_tree_entry_to_object(&obj, repo, mObject);
  THROW_IF_ERROR(err);
  return Tree::UPtr(new Tree((git_tree*)obj));
}

// ############################################################
// Tree
Tree::Tree(git_tree *tree)
  : StructWrapper<git_tree>(tree)
{
}

Tree::Tree(Tree && tree)
  : StructWrapper<git_tree> (tree.mObject)
{
  // steal
  mChildren = std::move(tree.mChildren);
  // erase
  tree.mObject = nullptr;
}

Tree & Tree::operator=(Tree && tree)
{
  if (this != &tree)
  {
    // steal
    mObject = tree.mObject;
    mChildren = std::move(tree.mChildren);
    // erase
    tree.mObject = nullptr;
  }
  return *this;
}

Tree::~Tree()
{
  if (mObject != nullptr)
    git_tree_free(mObject);
}

size_t Tree::size() const
{
  THROW_IF_UNINIT;
  return git_tree_entrycount(mObject);
}

void Tree::walk(walkCb cb) const
{
  if (cb == nullptr) THROW_ERROR("No callback given");

  int e = git_tree_walk(mObject, GIT_TREEWALK_PRE, walk_cb, &cb);
  THROW_IF_ERROR(e);
}

const Tree::Entry *Tree::get(size_t idx)
{
  EntryMapT::iterator it;
  it = mChildren.find(idx);
  if (it == mChildren.end())
  {
    mChildren[idx] = Tree::Entry(git_tree_entry_byindex(mObject, idx), this);
  }

  return &mChildren[idx];

}

} // namespace Git2Cpp
