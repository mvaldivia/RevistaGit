#ifndef GIT2CPP_REPOSITORY_H
#define GIT2CPP_REPOSITORY_H

#include <memory>
#include <string>
#include <stdexcept>
#include <vector>
#include <map>
#include "git2cpp_global.h"

#include "StructWrapper.h"
#include "Oid.h"
#include "Object.h"

extern "C" {
  struct git_repository;
}

namespace Git2Cpp {

class Reference;
class Commit;
class RevWalk;

class GIT2CPPSHARED_EXPORT Repository : public StructWrapper<git_repository>
{
public:
  Repository(git_repository * repository = nullptr);
  ~Repository();

  void open(const std::string & path);

  /* branch stuffs */
  std::string getCurrentBranch() const;

  /**
   * @brief getHead
   * @return pointer owned by caller
   */
  Reference getHead() const;

  /**
   * @brief getLocalBranches
   * @return
   */
  std::vector<Reference*> getLocalBranches() const;

  /**
   * @brief lookup an object type from an oid, throw if not found
   * @return Object type
   */
  template<class GObj>
  GObj lookup(const Oid & oid) const
  {
    throw std::runtime_error("Not implemented");
  }

private:
  typedef std::map<git_repository*, int> TRawPtrMap;
  static TRawPtrMap mRawPtrMap;
};

template<>
Object Repository::lookup<Object>(const Oid & oid) const;

template<>
Commit Repository::lookup<Commit>(const Oid & oid) const;


} // namespace Git2Cpp

#endif // GIT2CPP_REPOSITORY_H
