#ifndef GIT2CPP_GLOBAL_H
#define GIT2CPP_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(GIT2CPP_LIBRARY)
#  define GIT2CPPSHARED_EXPORT Q_DECL_EXPORT
#else
#  define GIT2CPPSHARED_EXPORT Q_DECL_IMPORT
#endif

#ifdef __GNUC__
#define NO_EXCEPT noexcept
#else
#define NO_EXCEPT
#endif

#endif // GIT2CPP_GLOBAL_H
