#include "Oid.h"
#include <git2/oid.h>
#include "Exception.h"

#include "GitDebug.h"

#include <string.h>

namespace Git2Cpp {

Oid::Oid()
  : mOid(new git_oid())
{
  memset(mOid.get(), 0, sizeof(git_oid));
}

Oid::Oid(const git_oid *src)
  : mOid(new git_oid())
{
  //DEBUG("Oid constructor");
  if (src)
    git_oid_cpy(mOid.get(), src);
  else
    memset(mOid.get(), 0, sizeof(git_oid));
}

Oid::Oid(const std::string &src)
  : mOid(new git_oid())
{
  //DEBUG("Oid constructor from string");
  git_oid oid;
  int err = git_oid_fromstr(&oid, src.c_str());
  THROW_IF_ERROR(err);
  git_oid_cpy(mOid.get(), &oid);
}

Oid::Oid(const Oid & oid)
  : mOid(new git_oid())
{
  //DEBUG("Oid copy constructor");
  if (this != &oid)
  {
    git_oid_cpy(mOid.get(), oid.mOid.get());
  }
}

Oid & Oid::operator=(const Oid &oid)
{
  //DEBUG("Oid assignment operator");
  if (this != &oid)
  {
    git_oid_cpy(mOid.get(), oid.mOid.get());
  }
  return *this;
}

Oid::Oid(Oid && oid)
  : mOid(nullptr)
{
  //DEBUG("Oid move constructor");
  mOid = std::move(oid.mOid);
}

Oid & Oid::operator=(Oid && oid)
{
  //DEBUG("Oid assignment operator");
  if (this != &oid)
  {
    mOid = std::move(oid.mOid);
  }
  return *this;
}

Oid::~Oid()
{
}

bool Oid::operator==(const Oid &oid) const
{
  return (git_oid_cmp(mOid.get(), oid.mOid.get()) == 0);
}

bool Oid::operator!=(const Oid &oid) const
{
  return !(*this == oid);
}

bool Oid::operator <(const Oid &oid) const
{
  return (git_oid_cmp(mOid.get(), oid.mOid.get()) < 0);
}

bool Oid::isNull() const
{
  if (mOid)
    return git_oid_iszero(mOid.get());
  else
    return true;
}

std::string Oid::toShortString() const
{
  char tmp[10] = {0};
  git_oid_nfmt(tmp, 9, mOid.get());
  return tmp;
}

std::string Oid::toString() const
{
  char tmp[GIT_OID_HEXSZ+1] = {0};
  git_oid_nfmt(tmp, GIT_OID_HEXSZ, mOid.get());
  return tmp;
}

const git_oid * Oid::operator*() const
{
  return mOid.get();
}

git_oid * Oid::operator*()
{
  return mOid.get();
}

} // namespace Git2Cpp
