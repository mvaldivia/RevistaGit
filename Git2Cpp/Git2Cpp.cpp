#include "Git2Cpp.h"

#include <sstream>
#include <git2/global.h>

namespace Git2Cpp {

void Initialize()
{
  git_libgit2_init();
}

void Uninitialize()
{
  git_libgit2_shutdown();
}

std::string Version()
{
  int major, minor, rev;
  git_libgit2_version(&major, &minor, &rev);

  std::ostringstream oss;
  oss << major << "." << minor << "." << rev;
  return oss.str();
}

#define SHOW_FEATURE(oss, feat, val) oss << " (" << ((feat & val) ? "" : "NO ") << #feat << ")"
std::string Description()
{
  std::ostringstream oss;
  oss << "Git2Cpp " << Git2Cpp::Version() << " with";
  int features = git_libgit2_features();
  if (features)
  {
    SHOW_FEATURE(oss, GIT_FEATURE_HTTPS, features);
    SHOW_FEATURE(oss, GIT_FEATURE_SSH, features);
    SHOW_FEATURE(oss, GIT_FEATURE_THREADS, features);
  }
  else
    oss << " no features";
  return oss.str();
}

} // namespace
