#ifndef GIT2CPP_REFERENCE_H
#define GIT2CPP_REFERENCE_H

#include <string>
#include <memory>
#include "git2cpp_global.h"
#include "StructWrapper.h"

extern "C" {
  struct git_reference;
}

namespace Git2Cpp {

class Oid;
class Repository;

class GIT2CPPSHARED_EXPORT Reference : public StructWrapper<git_reference>
{
public:
  typedef std::unique_ptr<Reference> UPtr;

  Reference(git_reference * ref = nullptr);
  // copy
  Reference(const Reference & reference) = delete;
  Reference & operator=(const Reference & reference) = delete;
  // move
  Reference(Reference && reference);
  Reference & operator=(Reference && reference);
  // destructor
  ~Reference();

  std::string shorthand() const;
  std::string fullName() const;
  std::string symbolicTarget() const;

  bool isSymbolic() const;
  bool isBranch() const;
  bool isNote() const;
  bool isRemote() const;
  bool isTag() const;

  Oid target() const;
  Repository getRepository() const;
};

} // namespace Git2Cpp

#endif // GIT2CPP_REFERENCE_H
