#ifndef STATUS_H
#define STATUS_H

#include "StructWrapper.h"
#include "EnumsBitMaskOperator.h"

#include <functional>

extern "C" {
struct git_status_list;
}

namespace Git2Cpp {

class Repository;

// ##################################################
enum class StatusFlags : unsigned {
  CURRENT = 0,
  INDEX_NEW        = (1u << 0),
  INDEX_MODIFIED   = (1u << 1),
  INDEX_DELETED    = (1u << 2),
  INDEX_RENAMED    = (1u << 3),
  INDEX_TYPECHANGE = (1u << 4),
  WT_NEW           = (1u << 7),
  WT_MODIFIED      = (1u << 8),
  WT_DELETED       = (1u << 9),
  WT_TYPECHANGE    = (1u << 10),
  WT_RENAMED       = (1u << 11),
  WT_UNREADABLE    = (1u << 12),
  IGNORED          = (1u << 14),
  CONFLICTED       = (1u << 15),
};
ENABLE_BITMASK_OPERATOR(StatusFlags)
#define FLAG_HAS_VALUE(flag, value) ((flag & value) == value)

enum class StatusOptions : unsigned {
  NONE                             = 0,
  INCLUDE_UNTRACKED                = (1u << 0),
  INCLUDE_IGNORED                  = (1u << 1),
  INCLUDE_UNMODIFIED               = (1u << 2),
  EXCLUDE_SUBMODULES               = (1u << 3),
  RECURSE_UNTRACKED_DIRS           = (1u << 4),
  DISABLE_PATHSPEC_MATCH           = (1u << 5),
  RECURSE_IGNORED_DIRS             = (1u << 6),
  RENAMES_HEAD_TO_INDEX            = (1u << 7),
  RENAMES_INDEX_TO_WORKDIR         = (1u << 8),
  SORT_CASE_SENSITIVELY            = (1u << 9),
  SORT_CASE_INSENSITIVELY          = (1u << 10),
  RENAMES_FROM_REWRITES            = (1u << 11),
  NO_REFRESH                       = (1u << 12),
  UPDATE_INDEX                     = (1u << 13),
  INCLUDE_UNREADABLE               = (1u << 14),
  INCLUDE_UNREADABLE_AS_UNTRACKED  = (1u << 15),
  DEFAULTS                         = INCLUDE_UNTRACKED | INCLUDE_IGNORED | RECURSE_UNTRACKED_DIRS
};
ENABLE_BITMASK_OPERATOR(StatusOptions)

enum class StatusShow {
  INDEX_AND_WORKDIR = 0,
  INDEX_ONLY = 1,
  WORKDIR_ONLY = 2
};

// ##################################################
class Status : public StructWrapper<git_status_list>
{
public:
  Status(const Repository * repository, StatusShow show = StatusShow::INDEX_AND_WORKDIR, StatusOptions options = StatusOptions::DEFAULTS);
  ~Status();

  // disable copy
  Status(const Status &) = delete;
  Status & operator=(const Status &) = delete;

  typedef std::function<int (std::string fileName, StatusFlags flags)> FStatusCallback;

  static void list(const Repository *repository, StatusShow show, StatusOptions opts, FStatusCallback callback);


};

} // namespace Git2Cpp

#endif // STATUS_H
