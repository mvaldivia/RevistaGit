#include "Signature.h"
#include <sstream>

namespace Git2Cpp {

std::string Signature::toString() const
{
  std::ostringstream oss;
  oss << name << " <" << email << ">";
  return oss.str();
}

} // namespace Git2Cpp
