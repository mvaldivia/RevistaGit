#ifndef GIT2CPP_STRUCTWRAPPER_H
#define GIT2CPP_STRUCTWRAPPER_H


namespace Git2Cpp {

template<typename G>
class StructWrapper
{
public:
  StructWrapper(G *obj = nullptr)
    : mObject(obj)
  {

  }

  virtual ~StructWrapper()
  {

  }

  G **ptrAddr()
  {
    return &mObject;
  }

  G *ptr() const
  {
    return mObject;
  }

  void setPtr(G *p)
  {
    mObject = p;
  }

protected:
  G *mObject;
};

} // namespace Git2Cpp

#endif // GIT2CPP_STRUCTWRAPPER_H
