#ifndef GIT2CPP_INDEX_H
#define GIT2CPP_INDEX_H

#include "git2cpp_global.h"
#include "StructWrapper.h"

extern "C" {
  struct git_index;
}

namespace Git2Cpp {

class GIT2CPPSHARED_EXPORT Index : public StructWrapper<git_index>
{
public:
  Index(git_index * index = nullptr);
  ~Index();

};

} // namespace Git2Cpp

#endif // GIT2CPP_INDEX_H
