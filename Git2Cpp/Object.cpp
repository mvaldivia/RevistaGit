#include "Object.h"
#include "git2/object.h"

#include "Oid.h"
#include "GitDebug.h"

namespace Git2Cpp {

Object::Object(git_object *obj)
  : StructWrapper<git_object>(obj)
{
}

Object::Object(const Object & obj)
  : StructWrapper<git_object>(nullptr)
{
  if (&obj != this)
  {
    DEBUG("GitObject copy constructor");
    git_object_dup(&mObject, obj.mObject);
  }
}

Object& Object::operator=(const Object & obj)
{
  if (&obj != this)
  {
    DEBUG("GitObject assignment operator");
    git_object_dup(&mObject, obj.mObject);
  }
  return *this;
}

Object::Object(Object && obj)
  : StructWrapper<git_object>(obj.mObject)
{
  // steal
  // erase
  obj.mObject = nullptr;
}

Object& Object::operator=(Object && obj)
{
  if (this != &obj)
  {
    // steal
    mObject = obj.mObject;
    // erase
    obj.mObject = nullptr;
  }
  return *this;
}

Object::~Object()
{
  if (mObject)
    git_object_free(mObject);
}

bool Object::operator==(const Object & obj)
{
  return (getId() == obj.getId());
}

bool Object::operator!=(const Object & obj)
{
  return !(*this == obj);
}

std::string Object::typeStr() const
{
  git_otype t = git_object_type(mObject);
  return git_object_type2string(t);
}

Object::Type Object::type() const
{
  Type t;
  switch (git_object_type(mObject)) {
  case GIT_OBJ_ANY:
    t = Type::ANY;
    break;
  case GIT_OBJ_COMMIT:
    t = Type::COMMIT;
    break;
  case GIT_OBJ_TREE:
    t = Type::TREE;
    break;
  case GIT_OBJ_BLOB:
    t = Type::BLOB;
    break;
  case GIT_OBJ_TAG:
    t = Type::TAG;
    break;
  default:
    throw Exception("Unknown GitObject::Type");
    break;
  }
  return t;
}

Oid Object::getId() const
{
  return git_object_id(mObject);
}

} // namespace Git2Cpp
