#ifndef GIT2CPP_COMMIT_H
#define GIT2CPP_COMMIT_H

#include <string>
#include "git2cpp_global.h"
#include "StructWrapper.h"
#include "Object.h"
#include "Signature.h"

extern "C" {
  struct git_commit;
}

namespace Git2Cpp {

class Oid;
class Tree;
class Repository;

class Commit : public StructWrapper<git_commit>
{
public:
  Commit(git_commit * commit = nullptr);
  Commit(Object &&obj);

  // copy
  Commit(const Commit &) = delete;
  Commit & operator=(const Commit &) = delete;
  // move
  Commit(Commit && commit);
  Commit & operator=(Commit && commit);

  ~Commit();

  Oid getId() const;
  Tree getTree() const;
  Repository getOwner() const;

  Signature getAuthor() const;
  Signature getCommitter() const;
  std::string getBody() const;
  std::string getMessage() const;
  std::string getSummary() const;
  std::string getDateTime() const;

};

} // namespace Git2Cpp

#endif // GIT2CPP_COMMIT_H
