#ifndef GIT2CPP_GITEXCEPTION_H
#define GIT2CPP_GITEXCEPTION_H

#include <string>
#include <exception>
#include "git2cpp_global.h"

namespace Git2Cpp {

class GIT2CPPSHARED_EXPORT Exception : public std::exception
{
public:
  Exception(int err);
  Exception(const std::string &msg);

  const char *what() const NO_EXCEPT override;
  int getGitError() const;

private:
  int mErr;
  int mKlass;
  std::string mMessage;
};

} // namespace Git2Cpp

#define THROW_ERROR(e) throw Git2Cpp::Exception(e)
#define THROW_IF_ERROR(e) if (e != 0) THROW_ERROR(e)

#endif // GIT2CPP_GITEXCEPTION_H
