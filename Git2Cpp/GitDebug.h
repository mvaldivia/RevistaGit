#ifndef GITDEBUG_H
#define GITDEBUG_H

#include <iostream>

#define DEBUG(x) std::cout << "DEBUG: " << x << std::endl

#endif // GITDEBUG_H
