#include "Exception.h"
#include <git2/errors.h>

namespace Git2Cpp {

Exception::Exception(int err)
  : std::exception()
{
  const git_error *e = giterr_last();
  this->mErr = err;
  this->mKlass = e->klass;
  this->mMessage = e->message;
}

Exception::Exception(const std::string &msg)
  : std::exception()
  , mErr(GIT_EUSER)
  , mKlass(GITERR_NONE)
  , mMessage(msg)
{
}

const char *Exception::what() const NO_EXCEPT
{
  return mMessage.c_str();
}

int Exception::getGitError() const
{
  return mErr;
}

} // namespace Git2Cpp
