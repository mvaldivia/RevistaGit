#include "Index.h"
#include <git2/index.h>

namespace Git2Cpp {

Index::Index(git_index *index)
  : StructWrapper<git_index>(index)
{

}

Index::~Index()
{
  if (mObject != nullptr)
  {
    git_index_free(mObject);
    mObject = nullptr;
  }
}

} // namespace Git2Cpp
