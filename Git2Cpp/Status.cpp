#include "Status.h"

#include <git2/diff.h>
#include <git2/strarray.h>
#include <git2/types.h>
#include <git2/status.h>

#include "Repository.h"

namespace Git2Cpp {
  StatusFlags ToStatusFlags(unsigned int flags)
  {
    StatusFlags f = StatusFlags::CURRENT;
    for (int i = 0; i < 16; i++)
    {
      if (flags & (1u << i))
        f |= static_cast<StatusFlags>(1u << i);
    }
    return f;
  }

}

extern "C" {

int funcGitStatusCb(const char * path, unsigned int flags, void * payload)
{
  Git2Cpp::Status::FStatusCallback callback = *(Git2Cpp::Status::FStatusCallback *)payload;
  callback(path, Git2Cpp::ToStatusFlags(flags));
  return 0;
}

}

namespace Git2Cpp {

// ######################################################
StatusOptions ToOptions(git_status_opt_t opt)
{
  StatusOptions o = StatusOptions::NONE;
  for (int i = 0; i < 16; i++)
  {
    if (opt & (1u << i))
      o |= static_cast<StatusOptions>(1u << i);
  }
  return o;
}

git_status_opt_t ToStatusOpt(StatusOptions opt)
{
  unsigned int o = 0;
  for (int i = 0; i < 16; i++)
  {
    StatusOptions tmp = static_cast<StatusOptions>(1u << i);
    if ((opt & tmp) == tmp)
      o |= (1u << i);
  }
  return static_cast<git_status_opt_t>(o);
}
// ######################################################
git_status_show_t ToStatusShow(StatusShow s)
{
  return static_cast<git_status_show_t>(s);
}
// ######################################################
void initStatusOptions(git_status_options * opt, const StatusShow & show, const StatusOptions & options)
{
  int rc = git_status_init_options(opt, GIT_STATUS_OPTIONS_VERSION);
  THROW_IF_ERROR(rc);
  opt->show = ToStatusShow(show);
  opt->flags = ToStatusOpt(options);
  //opt->flags |= GIT_STATUS_OPT_INCLUDE_UNTRACKED;
}


// ######################################################
Status::Status(const Repository *repository, StatusShow show, StatusOptions options)
  : StructWrapper<git_status_list>(nullptr)
{  
  //rc = git_status_list_new(&mObject, repository->ptr(), &opt);
}

Status::~Status()
{
  if (mObject)
  {
    git_status_list_free(mObject);
    mObject = nullptr;
  }
}

void Status::list(const Repository *repository, StatusShow show, StatusOptions options, FStatusCallback callback)
{
  git_status_options opts;
  initStatusOptions(&opts, show, options);

  int rc = git_status_foreach_ext(repository->ptr(), &opts, funcGitStatusCb, (void*)&callback);
  THROW_IF_ERROR(rc);
}

} // namespace Git2Cpp
