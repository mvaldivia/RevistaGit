#ifndef GIT2CPP_H
#define GIT2CPP_H

#include "git2cpp_global.h"

#include <string>

namespace Git2Cpp {

GIT2CPPSHARED_EXPORT void Initialize();
GIT2CPPSHARED_EXPORT void Uninitialize();

GIT2CPPSHARED_EXPORT std::string Version();
GIT2CPPSHARED_EXPORT std::string Description();

} // namespace Git2Cpp

#endif // GIT2CPP_H
