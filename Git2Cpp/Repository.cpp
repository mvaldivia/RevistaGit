#include "Repository.h"
#include "Exception.h"
#include "Reference.h"
#include "Commit.h"

#include <git2/repository.h>
#include <git2/errors.h>
#include <git2/object.h>
#include <git2/commit.h>
#include <git2/branch.h>

#include <iostream>

#define THROW_IF_UNINIT if (mObject == nullptr) throw Exception("Repository not initialized")

namespace Git2Cpp {

Repository::TRawPtrMap Repository::mRawPtrMap = Repository::TRawPtrMap();

Repository::Repository(git_repository *repository)
  : StructWrapper<git_repository> (repository)
{
  if (repository != nullptr)
  {
    TRawPtrMap::iterator it = mRawPtrMap.find(repository);
    if (it == mRawPtrMap.end())
      mRawPtrMap[repository] = 0;

    std::cerr << "adding repo ptr at " << std::hex << repository << std::endl;
    mRawPtrMap[repository] = mRawPtrMap[repository] + 1;
  }
}

Repository::~Repository()
{
  if (mObject != nullptr)
  {
    TRawPtrMap::iterator it = mRawPtrMap.find(mObject);
    if (it != mRawPtrMap.end())
    {
      int & count = mRawPtrMap[mObject];
      if (count > 1)
        count--;
      else
      {
        std::cerr << "deleteing repo ptr at " << std::hex << mObject << std::endl;
        git_repository_free(mObject);
        mRawPtrMap.erase(it);
      }
    }
    mObject = nullptr;
  }
}

void Repository::open(const std::string & path)
{
  int err = git_repository_open_ext(&mObject, path.c_str(), GIT_REPOSITORY_OPEN_NO_SEARCH, 0);
  THROW_IF_ERROR(err);
  mRawPtrMap[mObject] = 1;
  std::cerr << "adding repo ptr at " << std::hex << mObject << std::endl;
}

std::string Repository::getCurrentBranch() const
{
  THROW_IF_UNINIT;

  std::string res;

  try
  {
    Reference head(getHead());
    res = head.shorthand();
  }
  catch (Exception &ex)
  {
    switch (ex.getGitError()) {
    case GIT_EUNBORNBRANCH:
      res = "no branch";
      break;
    case GIT_ENOTFOUND:
      res = "no head";
      break;
    default:
      throw;
    }
  }

  return res;
}

Reference Repository::getHead() const
{
  THROW_IF_UNINIT;

  Reference head;
  int err = git_repository_head(head.ptrAddr(), mObject);
  if (err != GIT_OK)
  {
    THROW_ERROR(err);
  }
  return head;
}

std::vector<Reference*> Repository::getLocalBranches() const
{
  THROW_IF_UNINIT;

  git_branch_iterator *it;
  int err = git_branch_iterator_new(&it, mObject, GIT_BRANCH_LOCAL);
  THROW_IF_ERROR(err);

  std::vector<Reference*> result;
  git_branch_t type;

  while (!err)
  {
    Reference::UPtr ref(new Reference());

    err = git_branch_next(ref->ptrAddr(), &type, it);
    if (err == 0)
      result.push_back(ref.release());
  }

  git_branch_iterator_free(it);
  if (err != GIT_ITEROVER)
    THROW_ERROR(err);

  return result;
}

template <>
Object Repository::lookup<Object>(const Oid &oid) const
{
  THROW_IF_UNINIT;

  Object obj;
  int err = git_object_lookup(obj.ptrAddr(), mObject, *oid, GIT_OBJ_ANY);
  THROW_IF_ERROR(err);
  return obj;
}

template <>
Commit Repository::lookup<Commit>(const Oid &oid) const
{
  THROW_IF_UNINIT;

  Commit obj;
  int err = git_commit_lookup(obj.ptrAddr(), mObject, *oid);
  THROW_IF_ERROR(err);
  return obj;
}

} // namespace Git2Cpp
